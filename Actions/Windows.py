# Internal libs
from Actions import Generic


class ClickOpenStartMenuWin10(Generic.ClickImageTemplate):

    name = 'Click start menu to open it (Win 10)'
    description = 'Useful for measuring click latency, as it is a small action.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_10_start_button.jpg')
        self.set_parameter('validation image', value='win_10_start_menu_validation.jpg')


class ClickOpenStartMenuWin2012(Generic.ClickImageTemplate):

    name = 'Click start menu to open it (Win 2012)'
    description = 'Useful for measuring click latency, as it is a small action.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_2012_start_button.jpg')
        self.set_parameter('validation image', value='win_2012_start_menu_validation.jpg')


class ClickStartMenuWinUserMenu10(Generic.ClickImageTemplate):

    name = 'Click start menu user menu (Win 10)'
    description = 'Sign out button is located here.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_10_start_menu_user_menu.jpg')
        self.set_parameter('validation image', value='win_10_start_menu_user_menu_sign_out.jpg')


class ClickStartMenuWinUserMenu2012(Generic.ClickImageTemplate):

    name = 'Click start menu user menu (Win 2012)'
    description = 'Sign out button is located here.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_2012_start_menu_user_menu.jpg')
        self.set_parameter('validation image', value='win_2012_start_menu_user_menu_sign_out.jpg')


class ClickStartMenuSignOut10(Generic.ClickImageTemplate):

    name = 'Click start menu sign out (Win 10)'
    description = 'Click the sign out button located in the user menu of the start menu .'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_10_start_menu_user_menu_sign_out.jpg')
        self.set_parameter('validation image', value='win_10_start_menu_user_menu_sign_out.jpg')
        self.set_parameter('invert validation', value=True)


class ClickStartMenuSignOut2012(Generic.ClickImageTemplate):

    name = 'Click start menu sign out (Win 2012)'
    description = 'Click the sign out button located in the user menu of the start menu .'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_2012_start_menu_user_menu_sign_out.jpg')
        self.set_parameter('validation image', value='win_2012_start_menu_user_menu_sign_out.jpg')
        self.set_parameter('invert validation', value=True)


class ClickCloseStartMenuWin10(Generic.ClickImageTemplate):

    name = 'Click start menu to close it (Win 10)'
    description = 'Useful for measuring click latency, as it is a small action.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_10_start_button.jpg')
        self.set_parameter('validation image', value='win_10_start_menu_validation.jpg')
        self.set_parameter('invert validation', value=True)


class ClickCloseStartMenuWin2012(Generic.ClickImageTemplate):

    name = 'Click start menu to close it (Win 2012)'
    description = 'Useful for measuring click latency, as it is a small action.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_2012_start_button.jpg')
        self.set_parameter('validation image', value='win_2012_start_menu_validation.jpg')
        self.set_parameter('invert validation', value=True)


class ClickNotepadInStartMenuWin10(Generic.ClickImageTemplate):

    name = 'Click Notepad start menu icon (Win 10)'
    description = "Notepad can be used to measure typing latency"

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_10_start_menu_notepad_application.jpg')
        self.set_parameter('validation image', value='win_10_notepad_application_started_validation.jpg')


class ClickNotepadInStartMenuWin2012(Generic.ClickImageTemplate):

    name = 'Click Notepad start menu icon (Win 2012)'
    description = "Notepad can be used to measure typing latency"

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_2012_start_menu_notepad_application.jpg')
        self.set_parameter('validation image', value='win_2012_notepad_application_started_validation.jpg')


class ClickNotepadOnDesktopWin10(Generic.ClickImageTemplate):

    name = 'Click Notepad desktop icon (Win 10)'
    description = "Notepad can be used to measure typing latency"

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_10_desktop_notepad_application.jpg')
        self.set_parameter('validation image', value='win_10_notepad_application_started_validation.jpg')
        self.set_parameter('number of clicks', value=2)


class ClickNotepadOnDesktopWin2012(Generic.ClickImageTemplate):

    name = 'Click Notepad desktop icon (Win 2012)'
    description = "Notepad can be used to measure typing latency"

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_2012_desktop_notepad_application.jpg')
        self.set_parameter('validation image', value='win_2012_notepad_application_started_validation.jpg')
        self.set_parameter('number of clicks', value=2)


class ClickInternetExplorerInStartMenuWin10(Generic.ClickImageTemplate):

    name = 'Click Internet Explorer in start menu (Win 10)'
    description = "Notepad can be used to measure typing latency"

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_10_start_menu_iexplore_application.jpg')
        self.set_parameter('validation image', value='win_10_iexplore_application_started_validation.jpg')


class ClickInternetExplorerInStartMenuWin2012(Generic.ClickImageTemplate):

    name = 'Click Internet Explorer in start menu (Win 2012)'
    description = "Notepad can be used to measure typing latency"

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_2012_start_menu_iexplore_application.jpg')
        self.set_parameter('validation image', value='win_2012_iexplore_application_started_validation.jpg')


class ClickInternetExplorerAddressBarWin10(Generic.ClickImageTemplate):

    name = 'Click Internet Explorer address bar (Win 10)'
    description = "Required for entering a URL."

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_10_iexplore_address_bar.jpg')


class ClickInternetExplorerAddressBarWin2012(Generic.ClickImageTemplate):

    name = 'Click Internet Explorer address bar (Win 2012)'
    description = "Required for entering a URL."

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_2012_iexplore_address_bar.jpg')


class ExitApplicationWin10(Generic.ClickImageTemplate):

    name = 'Exit application (Win 10)'
    description = "Click the application 'x' button to close it."

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_10_application_exit.jpg')
        self.set_parameter('horizontal offset', value=20)
        self.set_parameter('vertical offset', value=10)


class ExitApplicationWin2012(Generic.ClickImageTemplate):

    name = 'Exit application (Win 2012)'
    description = "Click the application 'x' button to close it."

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_2012_application_exit.jpg')
        self.set_parameter('horizontal offset', value=20)
        self.set_parameter('vertical offset', value=10)


class ExitNotepadApplicationWin10(Generic.ClickImageTemplate):

    name = 'Exit Notepad application (Win 10)'
    description = "Click the Notepad 'x' button to close it, validate by finding the unsaved document warning."

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_10_application_exit.jpg')
        self.set_parameter('validation image', value='win_10_dialog_dont_save_button.jpg')
        self.set_parameter('horizontal offset', value=20)
        self.set_parameter('vertical offset', value=10)


class ExitNotepadApplicationWin2012(Generic.ClickImageTemplate):

    name = 'Exit Notepad application (Win 2012)'
    description = "Click the Notepad 'x' button to close it, validate by finding the unsaved document warning."

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_2012_application_exit.jpg')
        self.set_parameter('validation image', value='win_2012_dialog_dont_save_button.jpg')
        self.set_parameter('horizontal offset', value=20)
        self.set_parameter('vertical offset', value=10)


class ClickDontSaveButtonWin10(Generic.ClickImageTemplate):

    name = "Click 'don't save' button. (Win 10)"
    description = "Useful in several dialogs, for example when closing Notepad."

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_10_dialog_dont_save_button.jpg')
        self.set_parameter('validation image', value='win_10_dialog_dont_save_button.jpg')
        self.set_parameter('invert validation', value=True)


class ClickDontSaveButtonWin2012(Generic.ClickImageTemplate):

    name = "Click 'don't save' button. (Win 2012)"
    description = "Useful in several dialogs, for example when closing Notepad."

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='win_2012_dialog_dont_save_button.jpg')
        self.set_parameter('validation image', value='win_2012_dialog_dont_save_button.jpg')
        self.set_parameter('invert validation', value=True)


EXPORTED_ACTIONS = [ClickOpenStartMenuWin10, ClickOpenStartMenuWin2012, ClickStartMenuWinUserMenu10,
                    ClickStartMenuWinUserMenu2012, ClickStartMenuSignOut10, ClickStartMenuSignOut2012,
                    ClickCloseStartMenuWin10, ClickCloseStartMenuWin2012, ClickNotepadInStartMenuWin10,
                    ClickNotepadInStartMenuWin2012, ClickNotepadOnDesktopWin10, ClickNotepadOnDesktopWin2012,
                    ClickInternetExplorerInStartMenuWin10, ClickInternetExplorerInStartMenuWin2012,
                    ClickInternetExplorerAddressBarWin10, ClickInternetExplorerAddressBarWin2012, ExitApplicationWin10,
                    ExitApplicationWin2012, ExitNotepadApplicationWin10, ExitNotepadApplicationWin2012,
                    ClickDontSaveButtonWin10, ClickDontSaveButtonWin2012]
