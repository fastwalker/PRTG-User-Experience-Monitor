# Standard libs
import logging
import datetime


class Parameter(object):

    def __init__(self, name=None, description=None, type='string', value=None, locked=False, hidden=False):

        self.name = name
        self.description = description
        self.type = type
        self.value = value
        self.hidden = hidden
        if not self.value:
            if type == 'string':
                self.value = ''
            if type == 'int':
                self.value = 0
            if type == 'bool':
                self.value = False

        self.locked = locked


class Action(object):

    name = 'Action'
    description = 'Does something'
    parameters = []

    def __init__(self, channel_name=None, parameters=None, lock_parameters=False, retry=3):
        """
        A single action, such as clicking an image template on screen or sending key strokes. Actions save their
        execution and validation time when executed. This means that the run time of an action can only be calculated
        for actions that have validation set.
        Actions can have a channel_name set; the performance (run time) of each action sharing a channel name will
        be averaged and returned to PRTG as that channels' output. An example use case is adding five
        'click start menu actions' and binding them to the 'mouse click latency' channel. In that case,
        the average time from clicking the start menu until detecting the start menu will be returned to PRTG under the
        'mouse click latency' channel.
        If an action has no channel_name set, it will not be output to PRTG in any other channel than the default
        'total run time' channel. Unmeasured actions can be used to prepare other actions; for example opening the
        Word application and opening a blank template can be unmeasured actions, setting up the measured action of
        typing in Word to measure keyboard latency.
        :param channel_name: results of actions can be grouped to represent a PRTG channel (str)
        :param parameters: some actions require additional parameters, they can be passed as a dict (Dict)
        :param lock_parameters: prevent user from altering any of this actions' parameters, name or description (bool)
        :param retry: how many times to retry the action if it fails before exiting the run (int)
        """
        self.channel_name = channel_name
        if parameters:
            self.add_parameter(*parameters)

        self.base_action = self.name  # For when user creates custom actions, remember the base action name

        # When the user adds an action, its' type is instantiated. Because the parameters dict exists before
        # instantiation (so the gui can display the parameters before the users adds the actions), each custom parameter
        # must be remade, in order to avoid linked parameter values in actions that are added multiple times.
        new_parameters = []
        for parameter in self.parameters:
            new_parameters.append(Parameter(name=parameter.name, description=parameter.description,
                                            type=parameter.type, value=parameter.value, locked=parameter.locked))
        self.parameters = new_parameters

        self.executed_time = None  # Filled at run time to determine how long the action took to complete
        self.validated_time = None  # Filled at run time to determine how long the action took to complete

        self.locked = lock_parameters
        if lock_parameters:
            self.lock_parameters()

        self.retry = retry


    def run_time(self, unit='Milliseconds'):
        """
        Return the total run time (measurement starts having executed the action, and ends having validated it. Only
        actions that use validation can return a run time.
        :param unit: calculate run time in this unit, accepted values are 'Millisecond' and 'Second'
        :return (float)
        """
        if not self.executed_time or not self.validated_time:
            return None
        return (self.validated_time - self.executed_time).microseconds / (1000 if unit == 'Milliseconds' else 1000000)

    def execute(self, agent):
        """
        Execute and record the time when execution completed.
        :param agent: the UemAgent object that will execute the action (UemAgent)
        """
        self._execute(agent=agent)
        self.executed_time = datetime.datetime.now()
        logging.log(level=logging.DEBUG, msg='Executed action {0}'.format(self.name))

    def validate(self, agent):
        """
        Validate and record the time when validation completed.
        :param agent: the UemAgent object that will execute the action (UemAgent)
        """
        self._validate(agent)
        self.validated_time = datetime.datetime.now()
        logging.log(level=logging.DEBUG, msg='Validated action {0}'.format(self.name))

    def _execute(self, agent):
        """
        Custom code for executing the action goes here. Extend the classes under UemActions.Generic for frequent
        use cases, such as clicking an image template or sending keys.
        :param agent: the UemAgent object that will execute the action (UemAgent)
        """
        pass

    def _validate(self, agent):
        """
        Custom code for validating the action goes here. Extend the classes under UemActions.Generic for frequent
        use cases, such as clicking an image template.
        :param agent: the UemAgent object that will execute the action (UemAgent)
        """
        pass

    def lock_parameters(self):

        for parameter in self.parameters:
            parameter.locked = True

    def add_parameter(self, *parameters):

        for parameter in parameters:
            if isinstance(parameter, type):
                parameter = parameter()
            self.parameters.append(parameter)

    def get_parameter(self, name):

        for parameter in self.parameters:
            if parameter.name.lower() == name.lower():
                return parameter

    def set_parameter(self, name, value):

        self.get_parameter(name).value = value

    @property
    def validate_after_execution(self):

        return False
