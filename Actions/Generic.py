# Internal libs
from Actions import Protos
# Standard libs
import time


class PreActionDelayParameter(Protos.Parameter):

    def __init__(self, value=0, *args, **kwargs):

        super().__init__(name='Pre action delay', description='Seconds to delay action execution by.',
                         type='int', value=value, *args, **kwargs)


class PostActionDelayParameter(Protos.Parameter):

    def __init__(self, value=1, *args, **kwargs):

        super().__init__(name='Post action delay', description='Seconds to delay next action by.',
                         type='int', value=value, *args, **kwargs)


class PreValidationDelayParameter(Protos.Parameter):

    def __init__(self, value=0, *args, **kwargs):

        super().__init__(name='Pre validation delay', description='Seconds to delay after validation.',
                         type='int', value=value, *args, **kwargs)


class PostValidationDelayParameter(Protos.Parameter):

    def __init__(self, value=0, *args, **kwargs):

        super().__init__(name='Post validation delay', description='Seconds to delay before validation.',
                         type='int', value=value, *args, **kwargs)


class TemplateImageParameter(Protos.Parameter):

    def __init__(self, *args, **kwargs):

        super().__init__(name='Template image', description='File path to template image to click.',
                         type='string', *args, **kwargs)


class RestrictToWindowParameter(Protos.Parameter):

    def __init__(self, *args, **kwargs):

        super().__init__(name='Restrict to window', description='Find the image only in the window containing this name.',
                         type='string', *args, **kwargs)


class HorizontalOffsetParameter(Protos.Parameter):

    def __init__(self, value=0, *args, **kwargs):

        super().__init__(name='Horizontal offset', value=value, description="E.g. '10' moves right, -10 moves left.",
                         type='int', *args, **kwargs)


class VerticalOffsetParameter(Protos.Parameter):

    def __init__(self, value=0, *args, **kwargs):

        super().__init__(name='Vertical offset', value=value, description="E.g. '10' moves down, -10 moves up.",
                         type='int', *args, **kwargs)


class ValidationImageParameter(Protos.Parameter):

    def __init__(self, *args, **kwargs):

        super().__init__(name='Validation image', description='File path to template image to confirm.',
                         type='string', *args, **kwargs)


class InvertValidationParameter(Protos.Parameter):

    def __init__(self, value=False, *args, **kwargs):

        super().__init__(name='Invert validation', value=value, description='Validate template image is NOT on screen.',
                         type='bool', *args, **kwargs)


class NumberOfClicksParameter(Protos.Parameter):

    def __init__(self, value=1, *args, **kwargs):

        super().__init__(name='Number of clicks', description='Amount of times to click.', type='int', value=value,
                         *args, **kwargs)


class KeysParameter(Protos.Parameter):

    def __init__(self, *args, **kwargs):

        super().__init__(name='Keys', description='Keystrokes to send.', type='string', *args, **kwargs)


class ClickImageTemplate(Protos.Action):

    name = 'Click image template'
    description = 'Find the template image (e.g. a log in button) and click it.'
    parameters = [TemplateImageParameter(), RestrictToWindowParameter(), HorizontalOffsetParameter(),
                  VerticalOffsetParameter(), ValidationImageParameter(), NumberOfClicksParameter(),
                  InvertValidationParameter(), PreActionDelayParameter(), PostActionDelayParameter(),
                  PreValidationDelayParameter(), PostValidationDelayParameter()]

    def __init__(self, template_path=None, validation_path=None, inverse_validation=None, number_of_clicks=None,
                 *args, **kwargs):
        """
        Action that can be subclassed to easily create actions that click a template image through image recognition
        using CV2 (implemented in UemAgent).
        :param template_path: path to template image which will be clicked, e.g. image of OS start menu button (str)
        :param validation_path: path to template image used to validate the success of the action, e.g. image of opened OS start menu (str)
        :param inverse_validation: confirm the validation image is NOT on screen (e.g. for confirming an application has closed (Bool)
        :param number_of_clicks (int)
        """
        super().__init__(*args, **kwargs)
        if template_path:
            self.set_parameter('template image', template_path)
        if validation_path:
            self.set_parameter('validation image', validation_path)
        if inverse_validation:
            self.set_parameter('invert validation', True if inverse_validation else False)
        if number_of_clicks:
            self.set_parameter('number of clicks', number_of_clicks)

    def _execute(self, agent):

        time.sleep(int(self.get_parameter('pre action delay').value))

        agent.click_image_template(template_path=self.get_parameter('template image').value,
                                   number_of_clicks=self.get_parameter('number of clicks').value,
                                   wait_for_template=True,
                                   offset=(int(self.get_parameter('horizontal offset').value or 0),
                                           int(self.get_parameter('vertical offset').value or 0)),
                                   restrict_to_window=self.get_parameter('restrict to window').value or None)

        time.sleep(int(self.get_parameter('post action delay').value))

    def _validate(self, agent):

        time.sleep(int(self.get_parameter('pre validation delay').value))

        if not self.get_parameter('invert validation').value:
            agent.find_image_template(template_path=self.get_parameter('validation image').value,
                                      wait_for_template=True,
                                      restrict_to_window=self.get_parameter('restrict to window').value or None)
        else:
            agent.ensure_image_template_not_present(template_path=self.get_parameter('validation image').value,
                                                    wait_for_template_not_present=True,
                                                    restrict_to_window=self.get_parameter('restrict to window').value or None)

        time.sleep(int(self.get_parameter('post validation delay').value))

    @property
    def validate_after_execution(self):
        return True if self.get_parameter('validation image').value else False


class SendKeys(Protos.Action):

    name = 'Send keystrokes'
    description = 'Send the keys in the "keys" parameter as keystrokes.'
    parameters = [KeysParameter(), ValidationImageParameter(), PreActionDelayParameter(), PostActionDelayParameter(),
                  PreValidationDelayParameter(), PostValidationDelayParameter()]

    def __init__(self, keys=None, validation_path=None, *args, **kwargs):
        """
        Action that can be subclassed to easily create actions that send keyboard strokes.
        :param keys: keyboard strokes to send (str)
        """
        super().__init__(*args, **kwargs)
        if keys:
            self.get_parameter('keys').value = keys
        if validation_path:
            self.get_parameter('validation image').value = validation_path

    def _execute(self, agent):

        time.sleep(int(self.get_parameter('pre action delay').value))

        agent.send_keys(self.get_parameter('keys').value)

        time.sleep(int(self.get_parameter('post action delay').value))

    def _validate(self, agent):

        time.sleep(int(self.get_parameter('pre validation delay').value))

        agent.find_image_template(template_path=self.get_parameter('validation image').value, wait_for_template=True)

        time.sleep(int(self.get_parameter('post validation delay').value))

    @property
    def validate_after_execution(self):
        return True if self.get_parameter('validation image').value else False


class SendSecretKeys(SendKeys):

    name = 'Send secret key strokes'
    description = 'Useful for sending passwords. Same as regular "send key strokes", but the keys parameter hides your input. Consider enabling encryption on the configuration file to avoid storing plaintext passwords.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.get_parameter('keys').hidden = True


class SendLorumIpsumKeyStrokes(SendKeys):

    name = 'Send Lorum Ipsum key strokes'
    description = 'Input a Lorum Ipsum story by sending keyboard strokes. Useful for measuring keyboard latency.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('keys', value='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod')
        self.set_parameter('validation image', value='lorum_ipsum_validation.jpg')


class PressFunctionalKey(SendKeys):

    name = 'Send function key stroke'
    description = 'E.g. "enter", "F1", etc.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

    def _execute(self, agent):

        agent.press_key(self.get_parameter('keys').value)


class PressEnterKey(PressFunctionalKey):

    name = 'Send "enter" key stroke'
    description = 'Input an enter.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('keys', value='enter')

    def _execute(self, agent):

        agent.press_key(self.get_parameter('keys').value)


EXPORTED_ACTIONS = [ClickImageTemplate, SendKeys, SendSecretKeys, PressFunctionalKey, PressEnterKey,
                    SendLorumIpsumKeyStrokes]
