# Internal libs
from Actions import Generic


class ClickUsernameField(Generic.ClickImageTemplate):

    name = 'Click Citrix username field'
    description = 'Allows sending key strokes to the input field'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='citrix_username_field.jpg')
        self.set_parameter('horizontal offset', value=200)


class ClickPasswordField(Generic.ClickImageTemplate):

    name = 'Click Citrix password field'
    description = 'Allows sending key strokes to the input field'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='citrix_password_field.jpg')
        self.set_parameter('horizontal offset', value=200)


class ClickLoginButton(Generic.ClickImageTemplate):

    name = 'Click Citrix login button'
    description = 'Useful for measuring click latency, as it is a small action.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='citrix_login_button.jpg')


class ClickDesktop(Generic.ClickImageTemplate):

    name = 'Click Citrix desktop'
    description = 'Open a Citrix desktop.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='citrix_desktop.jpg')
        self.set_parameter('validation image', value='citrix_desktop_started.jpg')
        self.set_parameter('post validation delay', value=15)


class ClickUserMenu(Generic.ClickImageTemplate):

    name = 'Click Citrix user menu'
    description = 'Opens the user menu on the storefront web page; the log off button is placed there.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='citrix_user_menu.jpg')
        self.set_parameter('validation image', value='citrix_log_off.jpg')


class ClickLogoff(Generic.ClickImageTemplate):

    name = 'Click Citrix logoff'
    description = 'Logs out of the Citrix web page.'

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.set_parameter('template image', value='citrix_log_off.jpg')


EXPORTED_ACTIONS = [ClickUsernameField, ClickPasswordField, ClickLoginButton, ClickDesktop, ClickUserMenu, ClickLogoff]
