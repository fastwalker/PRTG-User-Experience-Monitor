# Standard libs
import datetime
import logging
from logging.handlers import RotatingFileHandler
import time
import os
# External libs
from paepy.ChannelDefinition import CustomSensorResult  # Baked into PRTGs' python


def run_and_return_results(path):
    """
    Request agent to run all actions once and return the results per channel, then output the results to PRTG as JSON.
    return: {channel_name_1: channel_run_time_1, channel_name_2: channel_run_time_2} results dict
    """
    logging.log(level=logging.INFO, msg='Starting script sensor script, prtg custom sensor python folder: {0}'.format(
        path))
    _clean_up(path)
    request_agent_to_run(path)
    results = await_agent_results(os.path.join(path, 'run_results'))
    output_results(results)
    return results


def request_agent_to_run(path):
    """
    Write a 'run_request' file to the PRTG custom script folder to signal the agent to run and leave a run_results
    object. Write the current time into the file, so the agent can determine whether the request is current or expired.
    """
    logging.log(level=logging.DEBUG, msg='Requesting agent to run in path: {0}'.format(path))
    if os.path.exists(os.path.join(path, 'run_request')):
        os.remove(os.path.join(path, 'run_request'))
    with open(os.path.join(path, 'run_request'), 'w') as ofile:
        ofile.write(str(datetime.datetime.now()))


def await_agent_results(path):
    """
    Wait for the agent to leave a run_results object containing each channel and its run time. Once it appears,
    parse it and return it as a dict.
    :return: {channel_name: (run_time, unit, warning_threshold, alarm_threshold)} dict of tuples
    """
    while True:
        if not os.path.exists(path):
            time.sleep(0.5)
        else:
            results = _parse_agent_result_file(path)
            _clean_up(path)
            break
    logging.log(level=logging.DEBUG, msg='Processed results {0}'.format(results))
    return results


def output_results(results):
    """
    Import the PRTG results object and fill it with the passed results. Then print the output for the PRTG
    custom script sensor to pick up.
    :param: results {channel_name: (run_time, unit, primary, warning_threshold, alarm_threshold)} dict of tuples
    """
    sensor = CustomSensorResult("Online")
    for channel_name, channel_values in results.items():
        value, unit, primary, warning_threshold, red_threshold = channel_values

        sensor.add_channel(channel_name=channel_name, unit=unit, value=value, is_limit_mode=True,
                           limit_max_warning=warning_threshold, limit_warning_msg='{0} degraded'.format(channel_name),
                           limit_max_error=red_threshold, limit_error_msg='{0} severely degraded'.format(channel_name),
                           primary_channel=True if primary else False)

    logging.log(level=logging.DEBUG, msg='Outputting result {0}'.format(sensor.get_json_result()))
    print(sensor.get_json_result())


def _parse_agent_result_file(path):
    """
    Parse the agent 'run_results' file and return it as a dict.
    :param path (str)
    :return: {channel_name: (run_time, unit, warning_threshold, alarm_threshold)} dict of tuples
    """
    results = {}

    with open(path, 'r') as ofile:
        text = ofile.readlines()
    logging.log(level=logging.DEBUG, msg='Received results {0}'.format(results))

    for line in text:
        #  Format per line: channel_name, channel_runtime, channel_unit, is_primary, warning_threshold, alarm_threshold
        #  e.g. 'Keyboard latency, 100, Milliseconds, 250, 500
        results[line.split(',')[0].strip()] = (line.split(',')[1].strip(), line.split(',')[2].strip(),
                                               line.split(',')[3].strip(), line.split(',')[4].strip(),
                                               line.split(',')[5].strip())
    return results


def _clean_up(path):
    """
    Remove the 'run_request' and 'run_results' files from the prtg custom sensor python path (safe if they already
    exist).
    """
    logging.log(level=logging.DEBUG, msg='Cleaning path: {0}'.format(path))
    if 'run_request' in os.listdir(path):
        os.remove(os.path.join(path, 'run_request'))
    if 'run_results' in os.listdir(path):
        os.remove(os.path.join(path, 'run_results'))


def _start_logging(log_path='./ScriptSensor.log', level=logging.DEBUG):
    """
    :param log_path (str)
    """
    if os.path.exists(log_path):
        os.remove(log_path)

    formatter = logging.Formatter('[%(asctime)s] [%(levelname)s] [%(module)s %(funcName)s(%(lineno)d)] %(message)s')
    handler = RotatingFileHandler(log_path, mode='w', maxBytes=20*1024*1024, backupCount=2, encoding=None, delay=0)
    handler.setFormatter(formatter)
    handler.setLevel(level=level)
    LOGGER = logging.getLogger('root')
    LOGGER.setLevel(level)
    LOGGER.addHandler(handler)


if __name__ == "__main__":

    _start_logging(level=logging.DEBUG)
    try:
        run_and_return_results(path=os.path.dirname(__file__))
    except Exception as e:
        logging.log(level=logging.ERROR, msg='Error during execution: {0}'.format(e))
        exit(-1)
    finally:
        _clean_up(os.path.dirname(__file__))
        exit(0)
