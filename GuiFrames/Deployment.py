# Standard libs
import tkinter as tk
import tkinter.ttk as ttk
# Internal libs
from GuiFrames import UemFrame
import HelperFuncs


class DeploymentFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Allow user to deploy sensor to PRTG, manage PRTG python requirements, deploy the script to the PRTG folder,
        save the configuration and chang the PRTG client path.
        """
        super().__init__(*args, **kwargs)
        self.logger.debug('Initializing deployment frame')

        self.prtg_deployment_frame = PrtgDeploymentFrame(self, client=self.client)
        self.python_deployment_frame = PythonDeploymentFrame(self, client=self.client)
        self.add_sensor_frame = AddSensorFrame(self, client=self.client)

        self.prtg_deployment_frame.grid(row=0, column=0, sticky=tk.W+tk.N+tk.S+tk.E)
        self.python_deployment_frame.grid(row=0, column=1, sticky=tk.W+tk.N+tk.S+tk.E)
        self.add_sensor_frame.grid(row=1, column=0, columnspan=2, sticky=tk.W+tk.N+tk.S+tk.E)

        self.prtg_deployment_frame.set_prtg_status()
        self.python_deployment_frame.set_python_requirement_status()
        self.prtg_deployment_frame.set_deployment_status()


class PrtgDeploymentFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Allow user to deploy sensor to PRTG and set client path.
        :param client: client window, i.e. (Gui object)
        """
        super().__init__(text='PRTG deployment', *args, **kwargs)
        self.logger.debug('Initializing PRTG deployment frame')

        self.prtg_path_entry_value = tk.StringVar(self)
        self.prtg_path_entry = tk.Entry(self, width=54, textvariable=self.prtg_path_entry_value)
        self.prtg_path_entry.insert(tk.END, self.client.config.prtg_root_folder)
        self.prtg_path_status_label = tk.Label(self, text='PRTG path valid:')
        self.prtg_path_status_canvas = tk.Canvas(self, height=5, width=5)

        self.script_deploy_script_button = tk.Button(self, text='Deploy script', state='disabled',
                                                     command=self.deploy_script_button_action)
        self.deployment_status_label = tk.Label(self, text='Sensor script deployed\n and up-to-date:', justify='left')
        self.deployment_status_canvas = tk.Canvas(self, height=5, width=5)

        self.prtg_path_entry.grid(row=0, column=0, columnspan=3, sticky=tk.W)
        self.prtg_path_status_label.grid(row=1, column=0, sticky=tk.W)
        self.prtg_path_status_canvas.grid(row=1, column=1, sticky=tk.W)
        self.deployment_status_label.grid(row=3, column=0, sticky=tk.W)
        self.deployment_status_canvas.grid(row=3, column=1, sticky=tk.W)

        self.script_deploy_script_button.grid(row=5, column=0, sticky=tk.W)

        self.prtg_path_entry_value.trace('w', self.on_prtg_path_entry_value_change)

    def on_prtg_path_entry_value_change(self, *args):

        self.client.config.prtg_root_folder = self.prtg_path_entry_value.get().strip()
        self.master.python_deployment_frame.prtg_python_path_entry.configure(state='normal')
        self.master.python_deployment_frame.prtg_python_path_entry.delete(0, tk.END)
        self.master.python_deployment_frame.prtg_python_path_entry.insert(tk.END, self.client.config.prtg_python_path)
        self.master.python_deployment_frame.prtg_python_path_entry.configure(state='disabled')
        self.set_prtg_status()
        self.master.python_deployment_frame.set_python_requirement_status()
        self.client.run_tab.agent_control_frame.set_requirements_met_status()

    def deploy_script_button_action(self, *args):

        HelperFuncs.deploy_python_sensor_script(
            prtg_custom_sensor_python_path=self.client.config.prtg_python_sensor_script_folder,
            uem_root_path=self.client.config.uem_root_folder)
        self.set_deployment_status()

    def set_prtg_status(self):
        """
        Change the path status label and canvas based on whether the current path is valid.
        """
        self.logger.debug('Setting PRTG path status')
        valid = HelperFuncs.prtg_root_path_is_valid(self.client.config.prtg_root_folder)
        self.prtg_path_status_canvas.config(bg='green' if valid else 'red')
        if valid and self.master.python_deployment_frame.requirement_status_canvas['bg'] == 'green':
            self.client.agent_frame.prtg_start_agent_button.configure(state='normal')

    def set_deployment_status(self):
        """
        Change the deployment status based on if SensorScript.py is found in the PRTG custom sensor python folder and if
        that script is equal to the template SensorScript.py in the UEM client folder.
        """
        self.logger.debug('Setting script sensor deployment status')
        deployed = HelperFuncs.sensor_script_is_deployed(self.client.config.prtg_root_folder)
        self.deployment_status_canvas.configure(bg='red' if not deployed else 'green')

        if not deployed and self.prtg_path_status_canvas['bg'] == 'green':
            self.script_deploy_script_button.configure(state='normal')
        else:
            self.script_deploy_script_button.configure(state='disabled')


class PythonDeploymentFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Allow user to install python requirements.
        """
        super().__init__(text='Python deployment', *args, **kwargs)
        self.logger.debug('Initializing python frame')

        self.prtg_python_path_entry = tk.Entry(self, width=56)
        self.prtg_python_path_entry.insert(tk.END, self.client.config.prtg_python_path)
        self.prtg_python_path_entry.configure(state='disabled')

        self.requirement_status_label = tk.Label(self, text='Python requirements installed:')
        self.pip_requirement_status_label = tk.Label(self, text='Pip installed:')
        self.pillow_requirement_status_label = tk.Label(self, text='Pillow (PIL) installed:')
        self.cv2_requirement_status_label = tk.Label(self, text='Opencv2-python installed:')
        self.pyautogui_requirement_status_label = tk.Label(self, text='PyAutoGUI installed:')
        self.dateutil_requirement_status_label = tk.Label(self, text='Dateutil installed:')
        self.requirement_status_canvas = tk.Canvas(self, height=5, width=5)
        self.pip_requirement_status_canvas = tk.Canvas(self, height=5, width=5)
        self.pillow_requirement_status_canvas = tk.Canvas(self, height=5, width=5)
        self.cv2_requirement_status_canvas = tk.Canvas(self, height=5, width=5)
        self.pyautogui_requirement_status_canvas = tk.Canvas(self, height=5, width=5)
        self.prepare_python_button = tk.Button(self, text='Install Python requirements',
                                               command=self.prepare_python_button_action, state='disabled')

        self.prtg_python_path_entry.grid(row=0, column=0, columnspan=3)
        self.requirement_status_label.grid(row=1, column=0, sticky=tk.W)
        self.requirement_status_canvas.grid(row=1, column=1, sticky=tk.W)
        self.pip_requirement_status_label.grid(row=2, column=0, sticky=tk.W, padx=(10, 0))
        self.pip_requirement_status_canvas.grid(row=2, column=1, sticky=tk.W)
        self.pillow_requirement_status_label.grid(row=3, column=0, sticky=tk.W, padx=(10, 0))
        self.pillow_requirement_status_canvas.grid(row=3, column=1, sticky=tk.W)
        self.cv2_requirement_status_label.grid(row=4, column=0, sticky=tk.W, padx=(10, 0))
        self.cv2_requirement_status_canvas.grid(row=4, column=1, sticky=tk.W)
        self.pyautogui_requirement_status_label.grid(row=5, column=0, sticky=tk.W, padx=(10, 0))
        self.pyautogui_requirement_status_canvas.grid(row=5, column=1, sticky=tk.W)
        self.prepare_python_button.grid(row=6, column=0, sticky=tk.W+tk.S+tk.N)

    def prepare_python_button_action(self):
        """
        Install missing python requirements.
        """
        #  todo: vcredit requirement
        self.logger.info('Preparing python @ {0}'.format(self.client.config.prtg_python_executable))

        progress_bar = ttk.Progressbar(self, mode='determinate')
        progress_bar.grid(row=6, column=1)
        HelperFuncs.python_requirements_are_valid(python_executable_path=self.client.config.prtg_python_executable,
                                                  pip_executable_path=self.client.config.prtg_python_pip_executable,
                                                  install_requirements=True, progress_bar=progress_bar)
        self.set_python_requirement_status()
        self.client.run_tab.agent_control_frame.set_requirements_met_status()
        progress_bar.destroy()

    def set_python_requirement_status(self):
        """
        Change the path status label and canvas based on whether the current path is valid.
        """
        self.logger.debug('Setting python requirement status')
        self.requirement_status_canvas.config(bg='green')

        if not HelperFuncs.prtg_root_path_is_valid(self.client.config.prtg_root_folder):
            self.prepare_python_button.configure(state='disabled')
            return [canvas.configure(bg='red') for canvas in [self.requirement_status_canvas,
                                                              self.pip_requirement_status_canvas,
                                                              self.pillow_requirement_status_canvas,
                                                              self.pyautogui_requirement_status_canvas,
                                                              self.cv2_requirement_status_canvas]]

        if HelperFuncs.pip_installed(pip_executable_path=self.client.config.prtg_python_pip_executable):
            self.pip_requirement_status_canvas.config(bg='green')
        else:
            self.prepare_python_button.configure(state='normal')
            return [canvas.configure(bg='red') for canvas in [self.requirement_status_canvas,
                                                              self.pip_requirement_status_canvas,
                                                              self.pillow_requirement_status_canvas,
                                                              self.pyautogui_requirement_status_canvas,
                                                              self.cv2_requirement_status_canvas]]

        pip_output = HelperFuncs.get_python_installed_packages(
            pip_executable_path=self.client.config.prtg_python_pip_executable)

        if HelperFuncs.python_pillow_installed(pip_executable_path=self.client.config.prtg_python_pip_executable,
                                               pip_output=pip_output):
            self.pillow_requirement_status_canvas.config(bg='green')
        else:
            self.pillow_requirement_status_canvas.config(bg='red')
            self.requirement_status_canvas.config(bg='red')

        if HelperFuncs.python_cv2_installed(pip_executable_path=self.client.config.prtg_python_pip_executable,
                                            pip_output=pip_output):
            self.cv2_requirement_status_canvas.config(bg='green')
        else:
            self.cv2_requirement_status_canvas.config(bg='red')
            self.requirement_status_canvas.config(bg='red')

        if HelperFuncs.python_pyautogui_installed(pip_executable_path=self.client.config.prtg_python_pip_executable,
                                                  pip_output=pip_output):
            self.pyautogui_requirement_status_canvas.config(bg='green')
        else:
            self.pyautogui_requirement_status_canvas.config(bg='red')
            self.requirement_status_canvas.config(bg='red')

        if self.requirement_status_canvas['bg'] == 'green':
            self.prepare_python_button.configure(state='disabled')
        else:
            self.prepare_python_button.configure(state='normal')


class AddSensorFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Allow user to install python requirements.
        """
        super().__init__(text='Deployment tutorial', *args, **kwargs)
        self.logger.debug('Initializing add sensor frame')

        text = \
        """
        1. Deploy the script to PRTG with the 'Deploy script' button above. It can now be used in a PRTG sensor;
        2. Install all missing dependencies to PRTGs' Python by clicking 'Install Python requirements' above;
        3. A PRTG script sensor must now be created. Log into PRTG and go to the 'Devices' tab;
        4. Right click the probe this is running on and click "Add sensor";
        5. Add a "Python script advanced" sensor;
        6. Choose the following settings (the rest may stay default):
             - Python script: "ScriptSensor.py" (deployed in step 1);
             - Scanning interval: 5 minutes (1 minute works for the default setup, but leaves little breathing room);
        7. If the agent is accepting requests through the '4. Run' tab, the sensor will start monitoring.
        """
        self.tutorial_label = tk.Label(self, justify='left', text=text)
        self.tutorial_label.grid(row=0, column=0, sticky=tk.W)
