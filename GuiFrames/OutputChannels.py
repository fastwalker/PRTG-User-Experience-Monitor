# Standard libs
import tkinter as tk
# Internal libs
from GuiFrames import UemFrame


class OutputChannelsFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Allow user to create and edit (output) channels that will be imported by PRTG, and show details of the
        currently selected channel to user.
        """
        super().__init__(*args, **kwargs)
        self.logger.debug('Initializing output channels frame')

        self.current_channels_frame = CurrentChannelsFrame(self, client=self.client)
        self.bound_actions_frame = BoundActionsFrame(self, client=self.client)
        self.channel_details_frame = ChannelDetailsFrame(self, client=self.client)

        self.current_channels_frame.grid(row=0, column=0, sticky=tk.W+tk.N+tk.S+tk.E)
        self.bound_actions_frame.grid(row=0, column=1, sticky=tk.W+tk.N+tk.S+tk.E)
        self.channel_details_frame.grid(row=1, column=0, columnspan=2, sticky=tk.N+tk.S+tk.W)

    def set_selected_channel(self, channel=None):
        """
        Load the details of the passed channel into the channel details frame.
        :param channel (ChannelConfig object)
        """
        self.channel_details_frame.set_selected_channel(channel)

    def get_selected_channel(self):

        return self.current_channels_frame.get_selected_channel()


class CurrentChannelsFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Allow user to create and edit (output) channels that will be imported by PRTG.
        """
        super().__init__(text='Current channels', *args, **kwargs)
        self.logger.debug('Initializing current channels frame')

        self.channels_listbox_scrollbar = tk.Scrollbar(self)
        self.channels_listbox = tk.Listbox(self, exportselection=0, height=15, width=35)
        self.channels_listbox.configure(yscrollcommand=self.channels_listbox_scrollbar.set)
        self.channels_listbox_scrollbar.configure(command=self.channels_listbox.yview)

        self.add_channel_button = tk.Button(self, text='Add new channel', command=self.add_channel_button_action)
        self.remove_channel_button = tk.Button(self, text='Remove channel', command=self.remove_channel_button_action,
                                               state='disabled')

        self.channels_listbox.pack(side='left', anchor=tk.NW, fill=tk.X)
        self.channels_listbox_scrollbar.pack(side='left', fill=tk.Y)
        self.add_channel_button.pack(side='top', anchor=tk.NW, fill=tk.X)
        self.remove_channel_button.pack(side='top', anchor=tk.NW, fill=tk.X)

        self.load_channels_listbox_items()
        if self.client.config.channels:
            self.channels_listbox.selection_set(0)

        self.channels_listbox.bind('<<ListboxSelect>>', self.on_current_channels_listbox_select)

    def on_current_channels_listbox_select(self, *args):
        """
        On select, load the selected channel details into the channel details frame.
        """
        if not self.channels_listbox.curselection():
            return
        channel = self.get_selected_channel()
        if channel.name.lower() == 'run total':
            self.remove_channel_button.configure(state='disabled')
        else:
            self.remove_channel_button.configure(state='normal')
        self.master.set_selected_channel(channel=channel)

    def add_channel_button_action(self):
        """
        On press: add a blank channel to Config.channels
        """
        new_channel = self.client.config.add_blank_channel()
        self.load_channels_listbox_items(selection=new_channel.name)
        self.master.set_selected_channel(new_channel)
        self.client.input_tab.action_details_frame.load_channel_dropdown_items()
        self.master.channel_details_frame.name_entry.focus()
        self.master.channel_details_frame.name_entry.select_range(0, tk.END)
        self.master.set_selected_channel(channel=new_channel)

    def remove_channel_button_action(self):
        """
        On press: remove channel from Config.channels
        """
        if not self.channels_listbox.curselection():
            return
        self.client.config.remove_channel(self.get_selected_channel())
        self.load_channels_listbox_items()
        self.client.input_tab.action_details_frame.load_channel_dropdown_items()

    def load_channels_listbox_items(self, selection=None):
        """
        Reload the listbox with the current channels from Config.channels. Optionally select the passed name if
        it matches a channel.
        :param selection: select this channel name after reload (str)
        """
        self.channels_listbox.delete(0, tk.END)

        for channel_name in self.client.config.channels.keys():
            self.channels_listbox.insert(tk.END, channel_name)
            if selection and selection == channel_name:
                self.channels_listbox.selection_set(tk.END)

        if not selection:
            self.channels_listbox.selection_set(0)

        self.channels_listbox.see(self.channels_listbox.curselection())

    def get_selected_channel(self):
        """
        :return: currently selected channel (ChannelObject)
        """
        index = self.channels_listbox.curselection()
        if index:
            index = index[0]
            channel_name = self.channels_listbox.get(index)
            return self.client.config.channels[channel_name]


class ChannelDetailsFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Show details of the selected channel to the user.
        """
        super().__init__(text='Channel details', *args, **kwargs)
        self.logger.debug('Initializing channel details frame')

        self.name_label = tk.Label(self, text='Name:')
        self.name_entry_value = tk.StringVar(self)
        self.name_entry = tk.Entry(self, textvariable=self.name_entry_value, width=55)

        self.unit_label = tk.Label(self, text='Unit:')

        self.warning_threshold_label = tk.Label(self, text='Warning threshold:')
        self.warning_threshold_entry_value = tk.StringVar(self)
        self.warning_threshold_entry = tk.Entry(self, textvariable=self.warning_threshold_entry_value, width=55)
        self.warning_message_label = tk.Label(self, text='Warning message:')
        self.warning_message_entry_value = tk.StringVar(self)
        self.warning_message_entry = tk.Entry(self, textvariable=self.warning_message_entry_value, width=55)

        self.alarm_threshold_label = tk.Label(self, text='Alarm threshold:')
        self.alarm_threshold_entry_value = tk.StringVar(self)
        self.alarm_threshold_entry = tk.Entry(self, textvariable=self.alarm_threshold_entry_value, width=55)
        self.alarm_message_label = tk.Label(self, text='Alarm message:')
        self.alarm_message_entry_value = tk.StringVar(self)
        self.alarm_message_entry = tk.Entry(self, textvariable=self.alarm_message_entry_value, width=55)

        self.unit_dropdown_value = tk.StringVar(self)
        self.unit_dropdown = tk.OptionMenu(self, self.unit_dropdown_value, 'Milliseconds', 'Seconds')
        self.unit_dropdown.configure(width=14)
        self.unit_dropdown_value.set('Milliseconds')

        self.channel_is_primary_label = tk.Label(self, text='Name:')
        self.primary_checkbox_value = tk.IntVar(self)
        self.primary_checkbox = tk.Checkbutton(self, text='Primary channel', variable=self.primary_checkbox_value)

        self.name_label.grid(row=0, column=0, sticky=tk.W)
        self.name_entry.grid(row=1, column=0, sticky=tk.W)
        self.warning_threshold_label.grid(row=2, column=0, sticky=tk.W)
        self.warning_threshold_entry.grid(row=3, column=0, sticky=tk.W)
        self.warning_message_label.grid(row=4, column=0, sticky=tk.W)
        self.warning_message_entry.grid(row=5, column=0, sticky=tk.W)
        self.alarm_threshold_label.grid(row=6, column=0, sticky=tk.W)
        self.alarm_threshold_entry.grid(row=7, column=0, sticky=tk.W)
        self.alarm_message_label.grid(row=8, column=0, sticky=tk.W)
        self.alarm_message_entry.grid(row=9, column=0, sticky=tk.W)
        self.unit_label.grid(row=10, column=0, sticky=tk.W)
        self.unit_dropdown.grid(row=11, column=0, sticky=tk.W)

        self.primary_checkbox.grid(row=12, column=0, sticky=tk.W)

        if self.client.config.channels:
            self.set_selected_channel(list(self.client.config.channels.values())[0])
        else:
            self.set_selected_channel(None)

        self.name_entry_value.trace('w', self.on_name_entry_change)
        self.warning_threshold_entry_value.trace('w', self.on_warning_threshold_entry_change)
        self.warning_message_entry_value.trace('w', self.on_warning_message_entry_change)
        self.alarm_threshold_entry_value.trace('w', self.on_alarm_threshold_entry_change)
        self.alarm_message_entry_value.trace('w', self.on_alarm_message_entry_change)
        self.unit_dropdown_value.trace('w', self.on_unit_dropdown_change)
        self.primary_checkbox_value.trace('w', self.on_is_primary_checkbox_change)

    def on_is_primary_checkbox_change(self, *args):
        """
        If a channel is made primary, set it to primary and remove primary from all other channels. If the channel was
        primary but was deselected, make the default channel ('Run total') primary.
        """
        channel = self.master.get_selected_channel()
        choice = self.primary_checkbox_value.get()
        if choice is not 0 and channel:
            self.client.config.set_primary_channel(channel)
            self.primary_checkbox.configure(state='disabled')

    def on_unit_dropdown_change(self, *args):
        """
        When user selects a channel for an action, set it in the Action object itself.
        """
        selected = self.unit_dropdown_value.get().strip()
        self.client.config.channels[self.name_entry_value.get().strip()].unit = selected

    def on_name_entry_change(self, *args):
        """
        If the name of a channel changes, do the following things:
            - Change it in Config.channels
            - Disallow the change if it results in identically named channels
            - Disallow the change if it results in an empty name
            - Change the name in the 'channel dropdown' element of the 'action details' frame.
            - Change the name channel_name property of actions that use the changed channel
        """
        channel = self.master.get_selected_channel()

        old_name = channel.name
        new_name = self.name_entry_value.get()

        # When the name field changes programmatically there is no selection, skip in that case
        if not channel:
            return

        # Nothing to do
        if new_name == old_name:
            return

        # If the name is empty or already exists, don't write it to config. Wait for user to enter a valid name
        if not new_name or new_name in [ch for ch in self.client.config.channels.keys()]:
            return

        self.client.config.rename_channel(channel=channel, new_name=new_name)
        self.master.current_channels_frame.load_channels_listbox_items(new_name)

        # If the currently displayed action has the channel set that just changed name, we need to update that name in
        # the 'choose channel dropbox' and reselect it to display the update.
        # Otherwise simply background update the dropbox.
        if not isinstance(self.client.input_tab.get_selected_action()[0], type) and \
                self.client.input_tab.get_selected_action()[0].channel_name == old_name:
            self.client.input_tab.action_details_frame.load_channel_dropdown_items(force_select=new_name)
        else:
            self.client.input_tab.action_details_frame.load_channel_dropdown_items()

    def on_warning_threshold_entry_change(self, *args):
        """
        If the value changes, update it in Config.channels.
        """
        channel = self.master.get_selected_channel()
        new_threshold = self.warning_threshold_entry_value.get().strip()
        if new_threshold is None or not new_threshold != channel.warning_threshold:
            return
        try:
            int(new_threshold)
        except:
            self.warning_threshold_entry_value.set(channel.warning_threshold)
        else:
            channel.warning_threshold = int(new_threshold)

    def on_warning_message_entry_change(self, *args):
        """
        If the value changes, update it in Config.channels.
        """
        channel = self.master.get_selected_channel()
        new_message = self.warning_message_entry_value.get().strip()
        if new_message is None or not new_message != channel.warning_message:
            return
        channel.warning_message = new_message

    def on_alarm_threshold_entry_change(self, *args):
        """
        If the value changes, update it in Config.channels.
        """
        channel = self.master.get_selected_channel()
        new_threshold = self.alarm_threshold_entry_value.get().strip()
        if new_threshold is None or not new_threshold != channel.warning_threshold:
            return
        try:
            int(new_threshold)
        except:
            self.alarm_threshold_entry_value.set(channel.alarm_threshold)
        else:
            channel.alarm_threshold = int(new_threshold)

    def on_alarm_message_entry_change(self, *args):
        """
        If the value changes, update it in Config.channels.
        """
        channel = self.master.get_selected_channel()
        new_message = self.alarm_message_entry_value.get().strip()
        if new_message is None or not new_message != channel.alarm_message:
            return
        channel.alarm_message = new_message

    def set_selected_channel(self, channel=None):
        """
        Load parameters of the channel into UI elements.
        :param channel (ChannelConfig object)
        """
        if channel:
            self.name_entry.config(state='normal')
            self.warning_threshold_entry.config(state='normal')
            self.alarm_threshold_entry.config(state='normal')
            self.unit_dropdown.configure(state='normal')
            self.name_entry_value.set(channel.name)
            self.unit_dropdown_value.set(channel.unit)
            self.warning_threshold_entry_value.set(channel.warning_threshold)
            self.warning_message_entry_value.set(channel.warning_message)
            self.alarm_threshold_entry_value.set(channel.alarm_threshold)
            self.alarm_message_entry_value.set(channel.alarm_message)
            self.primary_checkbox_value.set(1 if channel.primary else 0)
            self.primary_checkbox.configure(state='disabled' if channel.primary else 'normal')
            self.master.bound_actions_frame.load_bound_actions_listbox_items()
            if channel.name.lower() == 'run total':
                self.master.current_channels_frame.remove_channel_button.configure(state='disabled')
            else:
                self.master.current_channels_frame.remove_channel_button.configure(state='normal')
        else:
            self.name_entry.config(state='disabled')
            self.warning_threshold_entry.config(state='disabled')
            self.alarm_threshold_entry.config(state='disabled')
            self.unit_dropdown.configure(state='disabled')
            self.name_entry_value.set('')
            self.warning_threshold_entry_value.set('')
            self.alarm_threshold_entry_value.set('')
            self.unit_dropdown_value.set('')
            self.primary_checkbox_value.set(0)


class BoundActionsFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Shows the actions bound to the selected channel.
        """
        super().__init__(text='Actions currently bound to channel', *args, **kwargs)
        self.logger.debug('Initializing bound actions frame')

        self.action_indeces = {}

        self.bound_actions_listbox_scrollbar = tk.Scrollbar(self)
        self.bound_actions_listbox = tk.Listbox(self, width=42, height=15)
        self.bound_actions_listbox.configure(yscrollcommand=self.bound_actions_listbox_scrollbar.set)
        self.bound_actions_listbox_scrollbar.configure(command=self.bound_actions_listbox.yview)

        self.unbind_action_button = tk.Button(self, text='Unbind', command=self.unbind_action_button_action,
                                              width=9)

        self.bound_actions_listbox.pack(side='left', anchor=tk.NW, fill=tk.X)
        self.bound_actions_listbox_scrollbar.pack(side='left', fill=tk.Y)
        self.unbind_action_button.pack(side='top', anchor=tk.NW, fill=tk.X)

    def unbind_action_button_action(self, *args):

        selection = self.bound_actions_listbox.curselection()
        if not selection:
            return
        index = self.action_indeces[self.bound_actions_listbox.get(selection).strip()]
        self.client.config.actions[index].channel_name = None
        self.load_bound_actions_listbox_items()

    def load_bound_actions_listbox_items(self, *args):

        channel = self.master.current_channels_frame.get_selected_channel()
        if channel.name.lower() == 'run total':
            self.unbind_action_button.configure(state='disabled')
        else:
            self.unbind_action_button.configure(state='normal')

        self.bound_actions_listbox.delete(0, tk.END)
        self.action_indeces = {}

        index = 0
        for action in self.client.config.actions:
            if action.channel_name and action.channel_name.lower() == channel.name.lower() or \
                            channel.name.lower() == 'run total':
                self.bound_actions_listbox.insert(tk.END, action.name)
                self.action_indeces[action.name] = index
            index += 1

        if self.bound_actions_listbox.curselection():
            self.bound_actions_listbox.see(self.bound_actions_listbox.curselection())
