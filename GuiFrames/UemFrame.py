# Standard libs
import logging
import tkinter as tk


class UemFrame(tk.LabelFrame):

    def __init__(self, *args, client, **kwargs):
        """
        :param client: client window, i.e. (Gui object)
        """
        super().__init__(*args, **kwargs)
        self.client = client
        self.logger = logging.getLogger('root')

