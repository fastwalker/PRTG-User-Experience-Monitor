# Standard libs
import os
import threading
import tkinter as tk
from tkinter import messagebox
from tkinter import scrolledtext
# Internal libs
import Config
import HelperFuncs
from GuiFrames import UemFrame


class RunFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Allow user to start and stop the agent.
        :param client: client window, i.e. (Gui object)
        """
        super().__init__(*args, **kwargs)
        self.logger.debug('Initializing run frame')

        self.config_frame = ConfigFrame(self, client=self.client)
        self.template_frame = TemplateFrame(self, client=self.client)
        self.agent_settings_frame = AgentSettingsFrame(self, client=self.client)
        self.agent_control_frame = AgentControlFrame(self, client=self.client)
        self.last_results_frame = LastResultsFrame(self, client=self.client)

        self.config_frame.grid(row=0, column=0, sticky=tk.W+tk.E+tk.S+tk.N)
        self.template_frame.grid(row=1, column=0, columnspan=2, sticky=tk.W+tk.E+tk.S+tk.N)
        self.agent_settings_frame.grid(row=0, column=1, sticky=tk.W+tk.E+tk.S+tk.N)
        self.agent_control_frame.grid(row=0, column=2, sticky=tk.W+tk.E+tk.S+tk.N)
        self.last_results_frame.grid(row=1, column=2, sticky=tk.W+tk.E+tk.S+tk.N)


class ConfigFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Allow user to start and stop the agent.
        """
        super().__init__(text='Config', *args, **kwargs)

        self._last_config_check = None  # Cache for preventing continuous compare config calls

        self.logger.debug('Initializing config frame')

        self.config_saved_status_label = tk.Label(self, text='Config is saved:')
        self.config_saved_status_canvas = tk.Canvas(self, height=5, width=5)
        self.save_config_button = tk.Button(self, text='Save config', state='disabled',
                                            command=self.save_config_button_action)
        self.reset_config_button = tk.Button(self, text='Reset config', command=self.reset_config_button_action)
        self.encrypt_config_checkbox_value = tk.IntVar(self, value=1 if HelperFuncs.config_is_encrypted() else 0)
        self.encrypt_config_checkbox = tk.Checkbutton(self, variable=self.encrypt_config_checkbox_value,
                                                      text='Encrypt configuration file')

        self.encrypt_key_label = tk.Label(self, text='Encryption key:')
        self.encrypt_key_entry_value = tk.StringVar(self, value=self.client.decryption_key)
        self.encrypt_key_entry = tk.Entry(self, textvariable=self.encrypt_key_entry_value,
                                          state='normal' if HelperFuncs.config_is_encrypted() else 'disabled', show='*')

        self.config_saved_status_label.grid(row=0, column=0, sticky=tk.W)
        self.config_saved_status_canvas.grid(row=0, column=1, sticky=tk.W)

        self.save_config_button.grid(row=1, column=0, sticky=tk.W)
        self.reset_config_button.grid(row=2, column=0, pady=(0, 10), sticky=tk.W)
        self.encrypt_config_checkbox.grid(row=3, column=0, sticky=tk.W)
        self.encrypt_key_label.grid(row=4, column=0, sticky=tk.W)
        self.encrypt_key_entry.grid(row=5, column=0, sticky=tk.W)

        self.set_save_config_status()

        self.encrypt_config_checkbox_value.trace('w', self.on_encrypt_config_checkbox_value_change)
        self.encrypt_key_entry_value.trace('w', self.on_encrypt_key_entry_value_change)

    def on_encrypt_config_checkbox_value_change(self, *args):

        self.encrypt_key_entry.configure(state='disabled' if not self.encrypt_config_checkbox_value.get() else 'normal')
        self.set_save_config_status()

    def on_encrypt_key_entry_value_change(self, *args):

        self.set_save_config_status()

    def reset_config_button_action(self, *args):

        self.logger.info('Resetting config')
        self.client.reset_config()
        self.client.input_tab.current_actions_frame.load_current_actions_listbox_items()
        self.client.input_tab.available_actions_frame.set_selected_action(index=0)
        self.client.output_tab.current_channels_frame.load_channels_listbox_items()
        self.client.output_tab.channel_details_frame.set_selected_channel(self.client.config.channels['Run total'])

        self.set_save_config_status()

    def save_config_button_action(self, *args):

        self.logger.info('Saving config')
        encrypted = bool(self.encrypt_config_checkbox_value.get())
        if encrypted:
            if not self.encrypt_key_entry_value.get().strip():
                return messagebox.showerror("Encryption key not set",
                                            "An encryption key is required when encryption is enabled.")
            self.client.decryption_key = self.encrypt_key_entry_value.get()
        if not encrypted and 'Send secret key strokes' in [action.base_action for action in self.client.config.actions]:
            choice = messagebox.askyesno(
                'Enable encryption?',
                "There are actions containing hidden keys in your configuration file. Would you like to enable encryption to avoid storing sensitive data in plain text?")
            if choice:
                return self.encrypt_config_checkbox_value.set(1)
        self.client.config.config_to_json(encrypted=bool(encrypted), encryption_key=self.client.decryption_key or None)
        self.set_save_config_status()

    def set_save_config_status(self):
        """
        Change the config saved status based on if the current config and saved config are identical.
        """
        current_config = self.client.config.config_to_dict()
        saved_config = self.client.config.json_to_dict(decryption_key=self.client.decryption_key or None,
                                                       input_path=os.path.join(self.client.config.uem_root_folder,
                                                                               'config'))

        if self._last_config_check and tuple((current_config, saved_config)) == self._last_config_check:
            return

        self._last_config_check = (current_config, saved_config)
        configs_are_identical = self.client.config.compare_configs(current_config, saved_config)

        self.config_saved_status_canvas.configure(bg='red' if not configs_are_identical else 'green')

        if (not self.encrypt_config_checkbox_value.get() and HelperFuncs.config_is_encrypted()) or \
                (self.encrypt_config_checkbox_value.get() and not HelperFuncs.config_is_encrypted()):
            self.config_saved_status_canvas.configure(bg='red')

        self.save_config_button.configure(state='normal' if self.config_saved_status_canvas['bg'] == 'red' \
            else 'disabled')


class TemplateFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Allow user to start and stop the agent.
        """
        super().__init__(text='Config templates', *args, **kwargs)
        self.logger.debug('Initializing template frame')

        self.new_template_label = tk.Label(self)

        self.template_listbox_scrollbar = tk.Scrollbar(self)
        self.template_listbox = tk.Listbox(self, width=28)
        self.template_listbox.configure(yscrollcommand=self.template_listbox_scrollbar.set)
        self.template_listbox_scrollbar.configure(command=self.template_listbox.yview)

        self.load_template_button = tk.Button(self, text='Load from template', command=self.load_template_button_action)
        self.save_template_button = tk.Button(self, text='Save to template', command=self.save_template_button_action)
        self.delete_template_button = tk.Button(self, text='Delete template',
                                                command=self.delete_template_button_action)

        self.new_template_entry_value = tk.StringVar(self, value="New template")
        self.new_template_entry = tk.Entry(self, textvariable=self.new_template_entry_value)
        self.create_template_button = tk.Button(self, text='Create template', command=self.create_template_button_action)

        self.template_listbox.pack(side='left', anchor=tk.NW, fill=tk.BOTH)
        self.template_listbox_scrollbar.pack(side='left', fill=tk.Y)
        self.load_template_button.pack(side='top', anchor=tk.NW, fill=tk.X)
        self.save_template_button.pack(side='top', anchor=tk.NW, fill=tk.X)
        self.delete_template_button.pack(side='top', anchor=tk.NW, fill=tk.X)
        self.new_template_label.pack(side='top', anchor=tk.NW, fill=tk.X)
        self.new_template_entry.pack(side='top', anchor=tk.NW, fill=tk.X)
        self.create_template_button.pack(side='top', anchor=tk.NW, fill=tk.X)

        self.load_template_listbox_items()

    def load_template_button_action(self, *args):

        selection = self.template_listbox.curselection()
        template = self.template_listbox.get(selection).strip()
        self.client.config = Config.Config(load_from_path=os.path.join('./Templates', template))
        self.client.input_tab.current_actions_frame.load_current_actions_listbox_items()
        self.client.output_tab.current_channels_frame.load_channels_listbox_items()
        self.master.config_frame.set_save_config_status()

    def delete_template_button_action(self, *args):

        selection = self.template_listbox.curselection()
        template = self.template_listbox.get(selection).strip()
        os.remove(os.path.join('./Templates', template))
        self.load_template_listbox_items()

    def save_template_button_action(self, *args):

        selection = self.template_listbox.curselection()
        template = self.template_listbox.get(selection).strip()
        self.client.config.config_to_json(output_path=os.path.join('./Templates', template))

    def create_template_button_action(self, *args):

        template = self.new_template_entry_value.get().strip()
        self.client.config.config_to_json(output_path=os.path.join('./Templates', template))
        self.load_template_listbox_items()

    def load_template_listbox_items(self):

        self.template_listbox.delete(0, tk.END)
        valid_templates = []
        for path in os.listdir('./Templates'):
            try:
                Config.Config(load_from_path=os.path.join('./Templates', path))
            except:
                continue
            else:
                valid_templates.append(path)
        for template in valid_templates:
            self.template_listbox.insert(tk.END, template)

        if self.template_listbox.curselection():
            self.template_listbox.see(self.template_listbox.curselection())


class AgentSettingsFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Allow user to change agent settings.
        """
        super().__init__(text='Agent settings', *args, **kwargs)
        self.logger.debug('Initializing agent settings frame')

        self.timeout_tip_label = tk.Label(self, text='Recommended timeout is\nscan interval - 5 seconds')
        self.timeout_label = tk.Label(self, text='Timeout:')
        self.timeout_entry_value = tk.StringVar(self, value=self.client.config.agent_timeout)
        self.timeout_entry = tk.Entry(self, textvariable=self.timeout_entry_value)

        self.timeout_tip_label.grid(row=0, column=0, columnspan=2, sticky=tk.W)
        self.timeout_label.grid(row=1, column=0, sticky=tk.W)
        self.timeout_entry.grid(row=1, column=1, sticky=tk.W)

        self.timeout_entry_value.trace('w', self.on_timeout_entry_change)

    def on_timeout_entry_change(self, *args):

        selected = self.timeout_entry_value.get().strip()
        new_value = ''
        for char in selected:
            try:
                int(char)
            except ValueError:
                continue
            else:
                new_value += char

        if not new_value:
            return
        self.timeout_entry_value.set(new_value)
        self.client.config.agent_timeout = int(new_value)
        self.master.config_frame.set_save_config_status()


class AgentControlFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Allow user to start, stop and test the agent.
        """
        super().__init__(text='Agent control', *args, **kwargs)
        self.logger.debug('Initializing agent control frame')

        self.requirements_met_status_canvas = tk.Canvas(self, height=5, width=5)
        self.requirements_met_status_label = tk.Label(self, text='Requirements met:')

        self.accepting_requests_status_canvas = tk.Canvas(self, height=5, width=5)
        self.accepting_requests_status_label = tk.Label(self, text='Accepting PRTG run requests:')

        self.running_status_canvas = tk.Canvas(self, height=5, width=5)
        self.running_status_label = tk.Label(self, text='Currently running:')

        self.start_handling_requests_button = tk.Button(self, text='Start accepting run requests',
                                                        command=self.start_handling_requests_action, state='disabled')
        self.stop_handling_requests_button = tk.Button(self, text='Stop accepting run requests',
                                                       command=self.stop_handling_requests_action, state='disabled')

        self.test_run_button = tk.Button(self, text='Test run', command=self.test_run_action, state='disabled')
        self.stop_test_run_button = tk.Button(self, text='Stop test run', command=self.stop_test_run_action,
                                              state='disabled')

        self.requirements_met_status_label.grid(row=0, column=0, columnspan=2, sticky=tk.W)
        self.requirements_met_status_canvas.grid(row=0, column=2, sticky=tk.W)
        self.accepting_requests_status_label.grid(row=1, column=0, columnspan=2, sticky=tk.W)
        self.accepting_requests_status_canvas.grid(row=1, column=2, sticky=tk.W)
        self.running_status_label.grid(row=2, column=0, columnspan=2, sticky=tk.W)
        self.running_status_canvas.grid(row=2, column=2, sticky=tk.W)

        self.test_run_button.grid(row=3, column=0, sticky=tk.W)
        self.stop_test_run_button.grid(row=3, column=1, sticky=tk.E)
        self.start_handling_requests_button.grid(row=4, column=0, columnspan=2, sticky=tk.W+tk.E)
        self.stop_handling_requests_button.grid(row=5, column=0, columnspan=2, sticky=tk.W+tk.E)

        self._refresh()

    def _refresh(self):

        self.set_accepting_requests_status()
        self.set_running_status()
        self.set_test_run_button()
        self.set_handle_request_buttons()
        self.after(500, self._refresh)

    def set_requirements_met_status(self):

        self.set_requirements_met_status()
        self.requirements_met_status_canvas.configure(
            bg='red' if not self.run_requirements_met else 'green')

    def set_accepting_requests_status(self):

        self.accepting_requests_status_canvas.configure(
            bg='red' if not self.client.agent or not self.client.agent.handle_requests else 'green')

    def set_running_status(self):

        self.running_status_canvas.configure(
            bg='red' if not self.client.agent or not self.client.agent.running else 'green')

    def test_run_action(self):

        if not self.client.agent:
            self.client.initialize_agent()
        thread = threading.Thread(target=self.client.agent.run_once)
        thread.start()
        while not self.client.agent.running:
            continue
        self.set_test_run_button()
        self.set_handle_request_buttons()

    def stop_test_run_action(self):

        timeout = self.client.config.agent_timeout
        self.client.config.agent_timeout = 1
        while self.client.agent.running:
            continue
        self.client.config.agent_timeout = timeout
        self.set_test_run_button()
        self.set_handle_request_buttons()

    def start_handling_requests_action(self):

        if not self.client.agent:
            self.client.initialize_agent()
        self.client.agent.handle_requests = True
        self.set_test_run_button()
        self.set_handle_request_buttons()

    def stop_handling_requests_action(self):

        self.client.agent.handle_requests = False
        self.set_test_run_button()
        self.set_handle_request_buttons()

    def set_handle_request_buttons(self):
        """
        Only enable the start agent button if the agent is not running, the config is saved and all PRTGs' python
        meets all requirements.
        """
        self.start_handling_requests_button.configure(state='disabled')
        self.stop_handling_requests_button.configure(state='disabled')
        if self.client.agent and self.client.agent.handle_requests:
            self.stop_handling_requests_button.configure(state='normal')
            return

        if self.requirements_met_status_canvas['bg'].lower() == 'green' and self.client.agent \
                and not self.client.agent.running:
            self.start_handling_requests_button.configure(state='normal')

    def set_test_run_button(self):
        """
        Only enable the test run button if the agent is not running or accepting requests,
        the config is saved and all PRTGs' python meets all requirements.
        """
        self.test_run_button.configure(state='disabled')
        self.stop_test_run_button.configure(state='disabled')
        if (not self.client.agent and self.run_requirements_met) or \
                (self.client.agent and not self.client.agent.running and not self.client.agent.handle_requests):
            self.test_run_button.configure(state='normal')
        elif self.client.agent and self.client.agent.running and not self.client.agent.handle_requests:
            self.stop_test_run_button.configure(state='normal')

    @property
    def run_requirements_met(self):

        if self.client.deployment_tab.python_deployment_frame.requirement_status_canvas['bg'].lower() == 'green':
            return True
        else:
            return False


class LastResultsFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Allow user to view the results of the last run.
        """
        super().__init__(text='Last results', *args, **kwargs)
        self.logger.debug('Initializing last results frame')

        self.last_result_displayed = None
        self.results_text = scrolledtext.ScrolledText(self, height=15, width=36, state='disabled')
        self.results_text.pack(fill=tk.BOTH, side='top')

        self._refresh()

    def _refresh(self):

        self.load_last_results()
        self.after(1000, self._refresh)

    def load_last_results(self):

        self.results_text.configure(state='normal')
        if self.client.agent and self.client.agent.last_results and \
                self.client.agent.last_results != self.last_result_displayed:

            self.results_text.delete(0.0, tk.END)
            last_results = self.client.agent.last_results

            if isinstance(last_results, dict):
                for channel_name, channel_result in self.client.agent.last_results.items():
                    self.results_text.insert(tk.END, "{0}\t\t{1} {2}\n".format(
                        channel_name, channel_result, self.client.config.channels[channel_name].unit))
            else:
                self.results_text.insert(tk.END, 'Error during run: "{0}"'.format(last_results))

            self.last_result_displayed = last_results
        self.results_text.configure(state='disabled')
