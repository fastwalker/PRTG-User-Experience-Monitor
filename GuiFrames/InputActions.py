# Standard libs
import tkinter as tk
# Internal libs
from GuiFrames import UemFrame


class InputActionsFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Input frame showing list actions available to the user, the current list of actions added to the runbook,
        and details for the currently selection action.
        """
        super().__init__(*args, **kwargs)
        self.logger.debug('Initializing input actions frame')

        self.action_details_frame = ActionDetailsFrame(self, client=self.client)
        self.available_actions_frame = AvailableActionsFrame(self, client=self.client)
        self.current_actions_frame = CurrentActionsFrame(self, client=self.client)
        self.parameters_frame = ParametersFrame(self, client=self.client)

        self.available_actions_frame.grid(row=0, column=0, sticky=tk.N+tk.W+tk.E+tk.S, pady=(10, 0))
        self.current_actions_frame.grid(row=0, column=1, sticky=tk.N+tk.W+tk.E+tk.S, pady=(10, 0))
        self.action_details_frame.grid(row=1, column=0, sticky=tk.N+tk.W+tk.E+tk.S)
        self.parameters_frame.grid(row=1, column=1, sticky=tk.N+tk.W+tk.E+tk.S)

        self.available_actions_frame.set_selected_action(index=0)  # Default view

    def set_selected_action(self, action):
        """
        Show details of the passed action in the 'action details' sub frame.
        :param action (Action object)
        """
        self.action_details_frame.set_selected_action(action)

    def get_selected_action(self):
        """
        An action is always selected in the available actions frame or in the current actions frame, but not both.
        :return: currently selected action (Action object)
        """
        if self.available_actions_frame.available_actions_listbox.curselection():
            return self.available_actions_frame.get_selected_action()
        else:
            return self.current_actions_frame.get_selected_action()


class AvailableActionsFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Available actions frame showing list actions available to the user, and allowing user to add them to
        current actions.
        """
        super().__init__(text='Available actions', *args, **kwargs)
        self.logger.debug('Initializing available actions frame')

        self.description_label = tk.Label(self, text='These are all available actions')

        self.available_actions_listbox_scrollbar = tk.Scrollbar(self)
        self.available_actions_listbox = tk.Listbox(self, exportselection=0, width=42, height=15)
        self.available_actions_listbox.configure(yscrollcommand=self.available_actions_listbox_scrollbar.set)
        self.available_actions_listbox_scrollbar.configure(command=self.available_actions_listbox.yview)
        [self.available_actions_listbox.insert(tk.END, action.name) for action in self.client.config.all_actions]

        self.add_action_button = tk.Button(self, text='Add action', command=self.add_action_button_action)

        self.available_actions_listbox.pack(side='left', anchor=tk.NW, fill=tk.X)
        self.available_actions_listbox_scrollbar.pack(side='left', fill=tk.Y)
        self.add_action_button.pack(side='top', anchor=tk.NW, fill=tk.X)

        self.available_actions_listbox.bind('<<ListboxSelect>>', self.on_available_actions_listbox_select)

    def on_available_actions_listbox_select(self, *args):
        """
        On select, load the action details into the action details frame.
        """
        self.add_action_button.configure(state='normal')
        self.master.current_actions_frame.disable_action_buttons()
        self.master.current_actions_frame.current_actions_listbox.selection_clear(0, tk.END)
        if not self.available_actions_listbox.curselection():
            return

        action, index = self.get_selected_action()
        for available_action in self.client.config.all_actions:
            if available_action.name == action.name:
                return self.master.set_selected_action(available_action)

    def add_action_button_action(self):
        """
        Add the currently selection available action to the runbook (i.e. current actions).
        """
        action, index = self.get_selected_action()
        action = self.client.config.add_action(action)
        self.master.current_actions_frame.load_current_actions_listbox_items(select=tk.END)
        self.master.current_actions_frame.on_current_actions_listbox_select()
        self.master.set_selected_action(action)

    def set_selected_action(self, index):
        """
        Select the passed index in the listbox (i.e. make it blue on listbox and load action details in action
        details frame).
        :param index (int)
        """
        self.available_actions_listbox.selection_set(index)
        self.master.current_actions_frame.current_actions_listbox.selection_clear(0, tk.END)
        action, index = self.get_selected_action()
        self.master.set_selected_action(action)

    def get_selected_action(self):
        """
        Return the action current selection in the listbox.
        """
        index = self.available_actions_listbox.curselection()[0]
        if index is not None:
            return self.client.config.all_actions[index], index


class CurrentActionsFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Current actions frame showing list of actions currently added to the runbook, and allowing user to move them
        up and down list and remove them..
        """
        super().__init__(text='Current runbook', *args, **kwargs)
        self.logger.debug('Initializing current actions frame')

        self.current_actions_listbox_scrollbar = tk.Scrollbar(self)
        self.current_actions_listbox = tk.Listbox(self, exportselection=0, width=42, height=15)
        self.current_actions_listbox.configure(yscrollcommand=self.current_actions_listbox_scrollbar.set)
        self.current_actions_listbox_scrollbar.configure(command=self.current_actions_listbox.yview)

        self.move_to_top_action_button = tk.Button(self, text='Move to top', state='disabled',
                                                   command=self.move_to_top_button_action)
        self.move_up_action_button = tk.Button(self, text='Move up', state='disabled',
                                               command=self.move_up_action_button_action)
        self.move_down_action_button = tk.Button(self, text='Move down', state='disabled',
                                                 command=self.move_down_action_button_action)
        self.move_to_bottom_action_button = tk.Button(self, text='Move to bottom', state='disabled',
                                                      command=self.move_to_bottom_button_action)
        self.remove_action_button = tk.Button(self, text='Remove', state='disabled',
                                              command=self.remove_action_button_action)
        self.duplicate_action_button = tk.Button(self, text='Duplicate', state='disabled',
                                                 command=self.duplicate_button_action)

        self.current_actions_listbox.pack(side='left')
        self.current_actions_listbox_scrollbar.pack(side='left', fill=tk.Y)
        self.move_to_top_action_button.pack(side='top', anchor=tk.NW, fill=tk.X)
        self.move_up_action_button.pack(side='top', anchor=tk.NW, fill=tk.X)
        self.move_down_action_button.pack(side='top', anchor=tk.NW, fill=tk.X)
        self.move_to_bottom_action_button.pack(side='top', anchor=tk.NW, fill=tk.X)
        self.remove_action_button.pack(side='top', anchor=tk.NW, fill=tk.X)
        self.duplicate_action_button.pack(side='top', anchor=tk.NW, fill=tk.X)

        self.load_current_actions_listbox_items()

        self.current_actions_listbox.bind('<<ListboxSelect>>', self.on_current_actions_listbox_select)

    def on_current_actions_listbox_select(self, *args):
        """
        On select in listbox, load the action details into the action details frame.
        """
        self.master.available_actions_frame.add_action_button.configure(state='disabled')
        self.remove_action_button.configure(state='normal')
        self.duplicate_action_button.configure(state='normal')
        self.master.available_actions_frame.available_actions_listbox.selection_clear(0, tk.END)
        if not self.current_actions_listbox.curselection():
            return
        action, index = self.get_selected_action()

        if index == 0:
            self.move_to_top_action_button.configure(state='disabled')
            self.move_up_action_button.configure(state='disabled')
            self.move_down_action_button.configure(state='normal')
            self.move_to_bottom_action_button.configure(state='normal')
        elif index == len(self.client.config.actions) - 1:
            self.move_down_action_button.configure(state='disabled')
            self.move_to_bottom_action_button.configure(state='disabled')
            self.move_up_action_button.configure(state='normal')
            self.move_to_top_action_button.configure(state='normal')
        else:
            self.move_to_top_action_button.configure(state='normal')
            self.move_to_bottom_action_button.configure(state='normal')
            self.move_up_action_button.configure(state='normal')
            self.move_down_action_button.configure(state='normal')

        self.master.set_selected_action(action)

    def move_to_bottom_button_action(self, *args):
        """
        On press, move up (i.e. increase index) in Config.Actions.
        """
        action, index = self.get_selected_action()
        self.client.config.move_to_bottom(index)
        self.load_current_actions_listbox_items(select=len(self.client.config.actions) - 1)
        self.on_current_actions_listbox_select()

    def move_to_top_button_action(self, *args):
        """
        On press, move selected actions to end of Config.Actions.
        """
        action, index = self.get_selected_action()
        self.client.config.move_to_top(index)
        self.load_current_actions_listbox_items(select=0)
        self.on_current_actions_listbox_select()

    def move_up_action_button_action(self, *args):
        """
        On press move up selected action (i.e. increase index) in Config.Actions.
        """
        action, index = self.get_selected_action()
        self.client.config.move_up_action(index)
        self.load_current_actions_listbox_items(select=index - 1)
        self.on_current_actions_listbox_select()

    def move_down_action_button_action(self, *args):
        """
        On press, move down selected action (i.e. decrease index) in Config.Actions.
        """
        action, index = self.get_selected_action()
        self.client.config.move_down_action(index)
        self.load_current_actions_listbox_items(select=index + 1)
        self.on_current_actions_listbox_select()

    def duplicate_button_action(self, *args):
        """
        On press, move down (i.e. decrease index) in Config.Actions.
        """
        action, index = self.get_selected_action()
        self.client.config.duplicate_action(action)
        self.load_current_actions_listbox_items(select=index + 1)
        self.on_current_actions_listbox_select()

    def remove_action_button_action(self, *args):
        """
        On press, remove action from Config.Actions.
        """
        if not self.current_actions_listbox.curselection():
            return
        action, index = self.get_selected_action()
        del self.client.config.actions[index]
        if self.client.config.actions:
            self.load_current_actions_listbox_items(select=index - 1 if index > 0 else 0)  # try select action above
        else:
            self.load_current_actions_listbox_items()
            self.master.available_actions_frame.set_selected_action(index=0)
        self.on_current_actions_listbox_select()

    def load_current_actions_listbox_items(self, select=None, retain_selection=False):
        """
        (Re)load the current runbook actions into the listbox. Optionally selected an action by index, or retain
        the current selection.
        :param select: select this action index after reload (int)
        :param retain_selection: retain selection after reload (Bool)
        """
        selected_index, selected_action = None, None
        if retain_selection:
            selected_action, selected_index = self.get_selected_action()

        self.current_actions_listbox.delete(0, tk.END)
        for action in self.client.config.actions:
            self.current_actions_listbox.insert(tk.END, action.name)
        if select is not None:
            self.set_selected_action(index=select)

        if retain_selection and selected_index is not None:
            if selected_action not in self.client.config.actions:
                return self.master.available_actions_frame.set_selected_action(index=0)
            self.set_selected_action(index=selected_index)

        if self.current_actions_listbox.curselection():
            self.current_actions_listbox.see(self.current_actions_listbox.curselection())

    def disable_action_buttons(self):

        self.move_to_top_action_button.configure(state='disabled')
        self.move_to_bottom_action_button.configure(state='disabled')
        self.move_down_action_button.configure(state='disabled')
        self.move_up_action_button.configure(state='disabled')
        self.remove_action_button.configure(state='disabled')
        self.duplicate_action_button.configure(state='disabled')

    def enable_action_buttons(self):

        self.move_to_top_action_button.configure(state='normal')
        self.move_to_bottom_action_button.configure(state='normal')
        self.move_down_action_button.configure(state='normal')
        self.move_up_action_button.configure(state='normal')
        self.remove_action_button.configure(state='normal')
        self.duplicate_action_button.configure(state='normal')

    def set_selected_action(self, index):
        """
        Select the passed index in the listbox (i.e. make it blue on listbox and load action details in action
        details frame).
        :param index (int)
        """
        self.current_actions_listbox.selection_set(index)
        self.master.available_actions_frame.available_actions_listbox.selection_clear(0, tk.END)
        action, index = self.get_selected_action()
        self.master.set_selected_action(action)

    def get_selected_action(self):
        """
        Return the currently selection action object.
        :return (Action object)
        """
        index = self.current_actions_listbox.curselection()
        if index:
            index = index[0]
            return self.client.config.actions[index], index
        else:
            return None, None


class ActionDetailsFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Show details about the currently selected action and allow the user to edit them.
        """
        super().__init__(text='Selected action details', *args, **kwargs)
        self.logger.debug('Initializing action details frame')

        self.name_label = tk.Label(self, text='Name:')
        self.name_entry_value = tk.StringVar(self)
        self.name_entry = tk.Entry(self, state='disabled', textvariable=self.name_entry_value, width=42)

        self.description_label = tk.Label(self, text='Description:')
        self.description_text = tk.Text(self, state='disabled', wrap='word', height=7, width=31)

        self.channel_label = tk.Label(self, text='Output channel (optional):')
        self.channel_dropdown_value = tk.StringVar(self, value='None')
        self.channel_dropdown = tk.OptionMenu(self, self.channel_dropdown_value, ())

        self.name_label.grid(row=0, column=0, sticky=tk.W)
        self.name_entry.grid(row=1, column=0, sticky=tk.W)
        self.description_label.grid(row=2, column=0, sticky=tk.W)
        self.description_text.grid(row=3, column=0, sticky=tk.W + tk.S, columnspan=2, rowspan=4, pady=(0, 15),
                                   padx=(0, 58))
        self.channel_label.grid(row=7, column=0, sticky=tk.W)
        self.channel_dropdown.grid(row=8, column=0, sticky=tk.W, pady=(0, 15))

        self.load_channel_dropdown_items(force_select='None')

        self.channel_dropdown_value.trace('w', self.on_channel_dropdown_change)
        self.name_entry_value.trace('w', self.on_name_entry_value_change)

        self.after(500, self._check_description_text_change)

    def _check_description_text_change(self, *args):

        action, index = self.master.get_selected_action()
        if action and \
                self.description_text.get(0.0, tk.END).strip() != action.description:
            action.description = self.description_text.get(0.0, tk.END).strip()
        self.after(500, self._check_description_text_change)

    def on_name_entry_value_change(self, *args):

        action, index = self.master.get_selected_action()
        action.name = self.name_entry_value.get()
        self.master.current_actions_frame.load_current_actions_listbox_items(retain_selection=True)

    def on_channel_dropdown_change(self, *args):

        if not self.master.current_actions_frame.current_actions_listbox.curselection():
            return

        selected = self.channel_dropdown_value.get()

        if selected == 'None':
            selected = None
        action, index = self.master.current_actions_frame.get_selected_action()
        self.client.config.actions[index].channel_name = selected

    def load_channel_dropdown_items(self, force_select=None):
        """
        Load all channels into the dropdown. If an available action is selected, lock the dropdown, because channels
        can only be set for actions added to the runbook. Use force select to select the passed dropdown item; otherwise
        the currently selected action is selected automatically (or 'None' if it is an available actions).
        :param force_select: force selection to passed item (str)
        """
        selection = self.channel_dropdown_value.get().strip()
        self.channel_dropdown['menu'].delete(0, tk.END)
        self.channel_dropdown_values = \
            tuple(channel for channel in ['None'] +
                  [channel_name for channel_name in list(self.client.config.channels.keys())
                   if not channel_name.lower() == 'run total'])
        for choice in self.channel_dropdown_values:
            self.channel_dropdown['menu'].add_command(label=choice,
                                                      command=tk._setit(self.channel_dropdown_value, choice))

        if force_select and self.channel_dropdown['state'] != 'disabled':
            self.channel_dropdown_value.set(force_select)
        elif selection in self.channel_dropdown_values:
            self.channel_dropdown_value.set(selection)
        elif selection not in self.channel_dropdown_values:
            self.channel_dropdown_value.set('None')

        if len(self.client.config.channels) > 1:
            self.channel_dropdown.configure(state='normal')
        else:
            self.channel_dropdown.configure(state='disabled')

    def set_selected_action(self, action):
        """
        Load details of the passed action into the UI elements.
        :param action (Action object)
        """
        self.set_name(action)
        self.set_description(action)
        self.set_channel(action)
        self.master.parameters_frame.load_parameters_listbox_items()
        self.master.parameters_frame.parameters_listbox.selection_set(0)
        self.master.parameters_frame.on_parameters_listbox_selection()

    def set_name(self, action):

        self.name_entry.configure(state='normal')
        self.name_entry_value.set(action.name)
        if isinstance(action, type) or action.locked:
            self.name_entry.configure(state='disabled')

    def set_description(self, action):

        self.description_text.configure(state='normal')
        self.description_text.delete(0.0, tk.END)
        self.description_text.insert(tk.END, action.description)
        if isinstance(action, type) or action.locked:
            self.description_text.configure(state='disabled')

    def set_channel(self, action):

        if not isinstance(action, type) and len(self.client.config.channels) > 1:
            self.channel_dropdown_value.set(action.channel_name)
            self.channel_dropdown.configure(state='normal')
        else:
            self.channel_dropdown_value.set('None')
            self.channel_dropdown.configure(state='disabled')


class ParametersFrame(UemFrame.UemFrame):

    def __init__(self, *args, **kwargs):
        """
        Show details about the currently selected action and allow the user to edit them.
        """
        super().__init__(text='Selected action parameters', *args, **kwargs)
        self.logger.debug('Initializing parameters frame')

        self.parameters_listbox_scrollbar = tk.Scrollbar(self)
        self.parameters_listbox = tk.Listbox(self, width=42, exportselection=0)
        self.parameters_listbox.configure(yscrollcommand=self.parameters_listbox_scrollbar.set)
        self.parameters_listbox_scrollbar.configure(command=self.parameters_listbox.yview)

        self.parameter_description_label = tk.Label(self, wraplength=250, justify=tk.LEFT)

        self.parameter_value_entry_value = tk.StringVar(self)
        self.parameter_value_entry = tk.Entry(self, textvariable=self.parameter_value_entry_value, width=42,
                                              exportselection=0)

        self.parameter_value_checkbox_value = tk.StringVar(self)
        self.parameter_value_checkbox = tk.Checkbutton(self, variable=self.parameter_value_checkbox_value)

        self.parameters_listbox.grid(row=0, column=0, rowspan=10, columnspan=3, sticky=tk.W)
        self.parameters_listbox_scrollbar.grid(row=0, rowspan=10, column=4, sticky=tk.W+tk.N+tk.S)
        self.parameter_description_label.grid(row=12, column=0, sticky=tk.W)

        self.parameter_value_entry.grid(row=13, column=0, sticky=tk.W)
        self.parameter_value_entry.grid_remove()

        self.parameter_value_checkbox.grid(row=13, column=0, sticky=tk.W)
        self.parameter_value_checkbox.grid_remove()

        self.parameters_listbox.bind('<<ListboxSelect>>', self.on_parameters_listbox_selection)
        self.parameter_value_entry_value.trace('w', self.on_parameter_value_change)
        self.parameter_value_checkbox_value.trace('w', self.on_parameter_value_change)

    def load_parameters_listbox_items(self):

        self.parameters_listbox.delete(0, tk.END)
        action, index = self.master.get_selected_action()
        for parameter in action.parameters:
            self.parameters_listbox.insert(tk.END, parameter.name)
        if not self.parameters_listbox.curselection():
            self.parameters_listbox.selection_set(0)

    def on_parameters_listbox_selection(self, *args):

        action, index = self.master.get_selected_action()
        selection = self.parameters_listbox.curselection()
        if not action or not selection:
            return

        parameter_name = self.parameters_listbox.get(selection[0])
        for parameter in action.parameters:
            if parameter_name == parameter.name:

                self.parameter_description_label.configure(text=parameter.description)

                if parameter.type.lower() in ['string', 'int']:
                    if parameter.type.lower() == 'string':
                        if parameter.hidden:
                            self.parameter_value_entry.configure(show='*')
                            self.parameter_value_entry.update()
                        else:
                            self.parameter_value_entry.configure(show='')
                        self.parameter_value_entry_value.set(parameter.value)
                    else:
                        self.parameter_value_entry_value.set(parameter.value or 0)
                    self.parameter_value_entry.grid()
                    self.parameter_value_checkbox.grid_forget()
                    if isinstance(action, type) or parameter.locked:
                        self.parameter_value_entry.configure(state='disabled')
                    else:
                        self.parameter_value_entry.configure(state='normal')
                elif parameter.type.lower() == 'bool':
                    self.parameter_value_checkbox_value.set(1 if parameter.value else 0)
                    self.parameter_value_checkbox.grid()
                    self.parameter_value_entry.grid_remove()
                    if isinstance(action, type) or parameter.locked:
                        self.parameter_value_checkbox.configure(state='disabled')
                    else:
                        self.parameter_value_checkbox.configure(state='normal')

    def on_parameter_value_change(self, *args):

        action, index = self.master.get_selected_action()
        selection = self.parameters_listbox.curselection()
        if not action or not selection:
            return

        parameter_name = self.parameters_listbox.get(selection[0])
        for parameter in action.parameters:
            if parameter_name == parameter.name:

                if parameter.type.lower() == 'string':
                    parameter.value = self.parameter_value_entry_value.get().strip()
                elif parameter.type.lower() == 'int':
                    try:
                        parameter.value = int(self.parameter_value_entry_value.get())
                    except ValueError:
                        parameter.value = 0
                elif parameter.type.lower() == 'bool':
                    parameter.value = bool(int(self.parameter_value_checkbox_value.get()))
