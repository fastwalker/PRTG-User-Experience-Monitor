# Standard libs
import logging
import tkinter as tk
from tkinter import ttk, messagebox
# Internal libs
import Config
import HelperFuncs
from GuiFrames import InputActions, OutputChannels, Deployment, Run


class Client(tk.Tk):

    def __init__(self, *args, **kwargs):
        """
        Client GUI to allow easy use of UEM. Does the following for the user:
            - Allow to compose config file (or use the default config)
            - Prepare requirements (install python packages, deployment scripts to PRTG folder, etc.)
            - Allow to start and stop the agent accepting requests from PRTGs' script sensor
        The following frames are available:
            - Input actions (used for managing actions)
                - Available actions frame (used to add actions to the runbook)
                - Current actions (shows the actions that have been added and will be executed)
                - Action details (view and edit the parameters of the currently selected action)
            - Output channels (used for managing channels)
                - Current channels (shows the current channels)
                - Channel details (view and edit the parameters of the currently selected channel)
            - Deployment (used for managing requirements)
                - Manage PRTG client path
                - Install PRTG python requirements
                - Deploy the script sensor to PRTG
            - Agent (used to stop and start the agent)
                - Save the current config
                - Stop / start the agent from accepting requests from PRTGs' script sensor.
        """
        super().__init__(*args, **kwargs)

        HelperFuncs.start_logging()
        self.logger = logging.getLogger('root')
        HelperFuncs.prepare_uem_root_path()
        self.logger.info('Initializing GUI')

        self.agent = None  # Initialized JIT

        self.decryption_key = None
        if HelperFuncs.config_is_encrypted():
            self.ask_password()
            if self.decryption_key:
                self.config = Config.Config(decryption_key=self.decryption_key)
            else:
                self.reset_config()
        else:
            self.config = Config.Config()

        self.tabs = ttk.Notebook(self)
        self.input_tab = InputActions.InputActionsFrame(self.tabs, client=self)
        self.output_tab = OutputChannels.OutputChannelsFrame(self.tabs, client=self)
        self.deployment_tab = Deployment.DeploymentFrame(self.tabs, client=self)
        self.run_tab = Run.RunFrame(self.tabs, client=self)

        self.tabs.add(self.input_tab, text='1. Input actions')
        self.tabs.add(self.output_tab, text='2. Output channels')
        self.tabs.add(self.deployment_tab, text='3. Deployment')
        self.tabs.add(self.run_tab, text='4. Run')

        self.tabs.grid(row=0, column=0)
        self.title("User experience monitor 0.9")
        self.iconbitmap(r'./Images/doc/uem.ico')

        self.tabs.bind("<ButtonRelease-1>", self._on_tab_select)

    def _on_tab_select(self, *args):
        """
        If the deployment tab was selected, refresh the deployment and config status of that tab.
        """
        if self.tabs.tab(tk.CURRENT)['text'] == '1. Input actions':
            geometry = "{0}x{1}".format(self.input_tab.winfo_reqwidth(), self.input_tab.winfo_reqheight()+20)
            self.geometry(geometry)
        elif self.tabs.tab(tk.CURRENT)['text'] == '2. Output channels':
            geometry = "{0}x{1}".format(self.output_tab.winfo_reqwidth(), self.output_tab.winfo_reqheight()+20)
            self.geometry(geometry)
            self.output_tab.bound_actions_frame.load_bound_actions_listbox_items()
        elif self.tabs.tab(tk.CURRENT)['text'] == '3. Deployment':
            geometry = "{0}x{1}".format(self.deployment_tab.winfo_reqwidth(), self.deployment_tab.winfo_reqheight()+20)
            self.geometry(geometry)
        elif self.tabs.tab(tk.CURRENT)['text'] == '4. Run':
            geometry = "{0}x{1}".format(self.run_tab.winfo_reqwidth(), self.run_tab.winfo_reqheight()+20)
            self.geometry(geometry)
            self.run_tab.config_frame.set_save_config_status()

    def _on_password_change(self, dialog, *args):

        if not dialog.password_entry_value.get().strip():
            dialog.continue_button.configure(state='disabled')
        else:
            dialog.continue_button.configure(state='normal')
            self.decryption_key = dialog.password_entry_value.get()

    def _on_reset_encrypted_config(self, dialog, *args):

        self.decryption_key = None
        dialog.destroy()

    def _on_continue_encrypted_config(self, dialog, *args):

        if HelperFuncs.validate_decryption_key(key=dialog.password_entry_value.get()):
            dialog.destroy()
        else:
            messagebox.showerror("Incorrect decryption key", "The entered key could not be used to decrypt the configuration file. Please enter the correct decryption key.")

    def ask_password(self):

        toplevel = tk.Toplevel(self)
        toplevel.label = tk.Label(toplevel, text="Key to decrypt configuration file:")
        toplevel.password_entry_value = tk.StringVar(self)
        toplevel.password_entry = tk.Entry(toplevel, show='*', textvariable=toplevel.password_entry_value)
        toplevel.continue_button = tk.Button(toplevel, state='disabled',
                                             command=lambda *args: self._on_continue_encrypted_config(toplevel),
                                             text='Continue')
        toplevel.reset_config_button = tk.Button(toplevel,
                                                 command=lambda *args: self._on_reset_encrypted_config(toplevel),
                                                 text='Reset configuration')
        toplevel.label.grid(row=0, column=0, columnspan=2)
        toplevel.password_entry.grid(row=1, column=0, columnspan=2, sticky=tk.W+tk.E, pady=(0, 10))
        toplevel.continue_button.grid(row=2, column=0)
        toplevel.reset_config_button.grid(row=2, column=1)

        toplevel.password_entry_value.trace('w', lambda *args: self._on_password_change(toplevel))

        self.withdraw()
        self.wait_window(toplevel)
        self.deiconify()

    def initialize_agent(self):

        if self.agent:
            return
        try:
            self.logger.info('Initializing Agent')
            import Agent  # JIT
            self.agent = Agent.Agent(config=self.config)
        except Exception as e:
            self.logger.error("Error initializing agent: {0}".format(e))

    def reset_config(self):

        self.logger.error("Resetting config")
        self.config = Config.Config(default_config=True)

    def start(self):

        self.logger.info('Starting GUI')
        self.mainloop()


if __name__ == '__main__':

    gui = Client()
    gui.start()
