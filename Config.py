# Standard libs
import os
import json
import logging
from collections import OrderedDict
# Internal libs
import HelperFuncs
from Actions import Generic, Windows, Citrix


class Config(object):

    def __init__(self, load_from_path=None, default_config=False, agent_timeout=290,
                 prtg_root_folder='C:\Program Files (x86)\PRTG Network Monitor', decryption_key=None):
        """
        Representation of all configuration options necessary to run UEM and report to PRTG. Can be exported to-
        and imported from JSON. The following options are available:
            - Channels (channels that will be reported to the PRTG sensor) (OrderedDict)
                - Channel name (string)
                - Channel unit (Second / Millisecond) (string)
                - Channel warning threshold (int)
                - Channel alarm threshold (int)
            - Actions (list of actions that will be run by Agent) (list)
                - Action channel name (action performance is added to this channel) (string)
            - PRTG client folder (used by Script sensor deployment to deploy the script) (string)
        """
        self.logger = logging.getLogger('root')
        self.actions = []
        self.channels = OrderedDict({'Run total': ChannelConfiguration(name='Run total', unit='Seconds',
                                                                       primary=True)})  # Default channel

        self.agent_timeout = agent_timeout
        self.prtg_root_folder = prtg_root_folder
        self.uem_root_folder = os.path.abspath(os.path.dirname(__name__))

        # Try to load the passed config load_from_path. If none was passed, try to load %UEM_ROOT_PATH%/config.
        # If that does not # exist or if it is not valid, create a new default config and write it to
        # %UEM_ROOT_PATH%/config.
        if load_from_path and not default_config:
            try:
                self.json_to_config(load_from_path, decryption_key=decryption_key)
            except Exception as e:
                self.logger.error('Error loading passed config: {0}'.format(e))
                raise e
        elif not default_config:
            try:
                self.json_to_config(os.path.join(self.uem_root_folder, 'config'), decryption_key=decryption_key)
            except FileNotFoundError:
                self.logger.info('Config file not found, it will be created')
                self.config_to_json(os.path.join(self.uem_root_folder, 'config'))
            except Exception as e:
                self.logger.error('Error loading default config: "{0}", creating new one'.format(e))
        else:
            self.logger.info('Creating default config')
            self.config_to_json(os.path.join(self.uem_root_folder, 'config'))

    def add_action(self, action):
        """
        Append actions to the configuration.
        :param action: Action class / Action object
        """
        self.logger.debug('Adding action: "{0}"'.format(action.name))
        if isinstance(action, type):
            action = action()
        self.actions.append(action)
        return action

    def duplicate_action(self, action):
        """
        Duplicate actions and add it to the end of the configuration action list.
        :param action: Action class / Action object
        """
        self.logger.debug('Adding action: "{0}"'.format(action.name))
        if isinstance(action, type):
            action = action()
        self.actions.append(action)
        return action

    def move_to_top(self, index):
        """
        Move to top of execution order(i.e. decrease its' index in the Config.actions list)
        :param index: index of the action to move up (int)
        """
        action = self.actions[index]
        self.logger.debug('Moving to top "{0}"'.format(action.name))
        del self.actions[index]
        self.actions.insert(0, action)

    def move_to_bottom(self, index):
        """
        Move to bottom of execution order (i.e. decrease its' index in the Config.actions list)
        :param index: index of the action to move up (int)
        """
        action = self.actions[index]
        self.logger.debug('Moving action to bottom "{0}"'.format(action.name))
        del self.actions[index]
        self.actions.append(action)

    def move_up_action(self, index):
        """
        Move up in execution order (i.e. decrease its' index in the Config.actions list)
        :param index: index of the action to move up (int)
        """
        action = self.actions[index]
        self.logger.debug('Moving up action "{0}" @ index "{1}"'.format(action.name, index))
        del self.actions[index]
        self.actions.insert(index-1, action)

    def move_down_action(self, index):
        """
        Move down in execution order (i.e. increase its' index in the Config.actions list)
        :param index: index of the action to move up (int)
        """
        action = self.actions[index]
        self.logger.debug('Moving down action "{0}" @ index "{1}"'.format(action.name, index))
        del self.actions[index]
        self.actions.insert(index+1, action)

    def add_blank_channel(self):
        """
        Append a channel with default settings to the configuration.
        """
        self.logger.debug('Adding blank channel')
        name = 'New channel 1'

        # If the default name exists, keep incrementing the integer at the end of the name
        while name in self.channels.keys():
            new_int = int(name[-1]) + 1
            name = name[0:-1] + str(new_int)

        self.channels[name] = ChannelConfiguration(name=name)
        return self.channels[name]

    def remove_channel(self, channel):
        """
        Remove a channel (also removes the 'channel_name' property for each action that refers to the deleted channel).
        :param channel (Channel object)
        """
        self.logger.debug('Removing channel "{0}"'.format(channel.name))
        for action in self.actions:
            if action.channel_name == channel.name:
                action.channel_name = None
        del self.channels[channel.name]

    def rename_channel(self, channel, new_name):
        """
        Rename a channel (also renames the 'channel_name' property for each action that refers to the renamed channel).
        :param channel (Channel object)
        :param new_name (str)
        """
        self.logger.debug('Renaming channel "{0}" to "{1}'.format(channel.name, new_name))
        old_name = channel.name
        channel.name = new_name
        channels = self.channels
        self.channels = OrderedDict()
        for channel in channels.values():
            self.channels[channel.name] = channel

        for action in self.actions:
            if action.channel_name == old_name:
                action.channel_name = new_name

    def set_primary_channel(self, channel):
        """
        Rename a channel (also renames the 'channel_name' property for each action that refers to the renamed channel).
        :param channel (Channel object)
        """
        self.logger.debug('Setting primary channel "{0}"'.format(channel.name))
        for _channel in self.channels.values():
            if channel is not _channel:
                _channel.primary = False
        channel.primary = True

    @property
    def all_actions(self):
        """
        :return Return all available actions (list)
        """
        return Generic.EXPORTED_ACTIONS + Windows.EXPORTED_ACTIONS + Citrix.EXPORTED_ACTIONS

    @property
    def prtg_python_sensor_script_folder(self):
        """
        :return: path to PRTG python script sensor folder (str)
        """
        return os.path.join(self.prtg_root_folder, 'Custom Sensors/python')

    @property
    def prtg_python_path(self):
        """
        :return: path to python folder that ships with PRTG install (str)
        """
        return os.path.join(self.prtg_root_folder, 'Python34')

    @property
    def prtg_python_executable(self):
        """
        :return: path to python executable that ships with PRTG install (str)
        """
        return os.path.join(self.prtg_python_path, 'python.exe')

    @property
    def prtg_python_pip_executable(self):
        """
        :return: path to pip executable of python that ships with PRTG install, not installed by default (str)
        """
        return os.path.join(self.prtg_python_path, 'scripts/pip.exe')

    def compare_configs(self, config_dict_1, config_dict_2):
        """
        Compare two config dictionaries and determine whether they are equal.
        :param config_dict_1:
        :param config_dict_2:
        :return:
        """
        self.logger.debug('Comparing configs:\n"{0}"\n{1}'.format(config_dict_1, config_dict_2))
        # Check equal number of channels and actions in both configs
        if len(config_dict_1['config']['actions']) != len(config_dict_2['config']['actions']) or \
                len(config_dict_1['config']['channels']) != len(config_dict_2['config']['channels']):
            return False

        # Check equal action configs
        for action_1_name, action_1_dict in config_dict_1['config']['actions'].items():
            if action_1_name not in config_dict_2['config']['actions'].keys():
                return False

            action_2_dict = config_dict_2['config']['actions'][action_1_name]

            if not action_1_dict['channel_name'] == action_2_dict['channel_name'] or \
               not action_1_dict['base_action'] == action_2_dict['base_action'] or \
               not action_1_dict['description'] == action_2_dict['description']:
                    return False

            for parameter_1_name, parameter_1_dict in action_1_dict['parameters'].items():
                if parameter_1_name not in action_2_dict['parameters'].keys():
                    return False
                parameter_2_dict = action_2_dict['parameters'][parameter_1_name]
                if not parameter_1_dict['value'] == parameter_2_dict['value']:
                    return False

        # Check equal channel configs
        for channel_1_name, channel_1_dict in config_dict_1['config']['channels'].items():
            if channel_1_name in config_dict_2['config']['channels'].keys():
                if not config_dict_2['config']['channels'][channel_1_name]['unit'] == channel_1_dict['unit']:
                    return False
                if not config_dict_2['config']['channels'][channel_1_name]['warning_threshold'] == \
                        channel_1_dict['warning_threshold']:
                    return False
                if not config_dict_2['config']['channels'][channel_1_name]['alarm_threshold'] == \
                        channel_1_dict['alarm_threshold']:
                    return False
            else:
                return False

        # Check equal agent settings
        if not config_dict_1['config']['agent_timeout'] == config_dict_2['config']['agent_timeout']:
            return False
        if not config_dict_1['config']['prtg_root_folder'] == config_dict_2['config']['prtg_root_folder']:
            return False

        return True

    def config_to_json(self, output_path='./config', encrypted=False, encryption_key=None):
        """
        Dump the current configuration to JSON.
        """
        config_dict = self.config_to_dict()
        self.logger.debug('Writing config dict to json: {0}'.format(config_dict))
        self.dict_to_json(output_path=output_path, config_dict=config_dict, encrypted=encrypted,
                          encryption_key=encryption_key)

    def json_to_config(self, input_path='./config', decryption_key=None):
        """
        Load the current configuration from JSON.
        """
        self.logger.debug('Loading config dict from JSON: {0}'.format(input_path))

        config_dict = self.json_to_dict(input_path=input_path, decryption_key=decryption_key)
        self.dict_to_config(config_dict)

    def config_to_dict(self):
        """
        Return the current config as a dictionary.
        :return (dict)
        """
        config = {
            'config':
                {
                    'channels':
                        {
                            channel.name: channel.to_json() for channel in self.channels.values()
                        },
                    'actions':
                        {
                            "{0}_{1}".format(self._find_action_index(action), action.name):
                                ActionConfiguration(action).to_json()
                            for action in self.actions
                        },
                    'agent_timeout': self.agent_timeout,
                    'prtg_root_folder': self.prtg_root_folder
                }
        }
        self.logger.debug('Returning config as dict: {0}'.format(config))
        return config

    def dict_to_config(self, config_dict):
        """
        Load the passed dict as current config.
        :param config_dict (dict)
        """
        self.logger.debug('Loading config dict: {0}'.format(config_dict))
        if config_dict['config']['actions']:
            self.actions = [i for i in range(0, len(config_dict['config']['actions'].items()))]
        self.channels = {}

        self.agent_timeout = config_dict['config']['agent_timeout']
        self.prtg_root_folder = config_dict['config']['prtg_root_folder']
        for channel_name, channel_config in config_dict['config']['channels'].items():
            self.channels[channel_name] = ChannelConfiguration(name=channel_name,
                                                               unit=channel_config['unit'],
                                                               primary=True if channel_config['primary'] else False,
                                                               warning_threshold=channel_config['warning_threshold'],
                                                               warning_message=channel_config['warning_message'],
                                                               alarm_threshold=channel_config['alarm_threshold'],
                                                               alarm_message=channel_config['alarm_message'])

        for action_name, action_config in config_dict['config']['actions'].items():
            action_index = action_name.split('_')[0]
            action_name = action_name.split('_')[1]
            for available_action in self.all_actions:
                if action_config['base_action'] == available_action.name:
                    action = available_action()
                    action.name = action_name
                    action.description = action_config['description']
                    if action_config['channel_name'] != 'null':
                        action.channel_name = action_config['channel_name']
                    if action_config['parameters'] != 'null':
                        for parameter_name, parameter_config in action_config['parameters'].items():
                            parameter_name = parameter_name.split('_')[1]
                            action.set_parameter(parameter_name, parameter_config['value'])

                    self.actions[int(action_index)] = action

    def dict_to_json(self, config_dict, output_path='./config', encrypted=False, encryption_key=None):
        """
        Write a config dict to a JSON file.
        :param config_dict (dict)
        :param output_path (str)
        :return: config dict
        """
        self.logger.debug('Writing config dict to json: {0}'.format(config_dict, output_path))
        if os.path.exists(output_path):
            os.remove(output_path)

        data = json.dumps(obj=config_dict)
        if encrypted:
            HelperFuncs.write_encrypted_file(data_string=data, key_string=encryption_key, path=output_path)
        else:
            with open(output_path, 'w') as ofile:
                json.dump(obj=config_dict, fp=ofile)

    def json_to_dict(self, input_path='./config', decryption_key=None):
        """
        Open a JSON file and return it as a dict.
        :param input_path (str)
        :return: config dict
        """
        if decryption_key:
            data = HelperFuncs.read_encrypted_file(key_string=decryption_key, path=input_path)
        else:
            with open(input_path, 'rb') as ofile:
                data = ofile.read()
        config_dict = json.loads(data.decode())
        self.logger.debug('Loading JSON file as dict: {0}'.format(input_path, config_dict))
        return config_dict

    def _find_action_index(self, action):
        """
        Find the index of the passed action in Config.Actions.
        :param action (Action object)
        :return: index (int)
        """
        self.logger.debug('Finding index for action: "{0}"'.format(action.name))
        for index, _action in enumerate(self.actions):
            if action is _action:
                return index


class ChannelConfiguration(object):

    def __init__(self, name='New channel', unit='Milliseconds', primary=False, warning_threshold=0,
                 warning_message=None, alarm_threshold=0, alarm_message=None):
        """
        Representation of a complete channel config. Used to store channels in Config.channels.
        :param name (str)
        :param unit: 'Milliseconds' or 'Seconds'
        :param warning_threshold: PRTG sensor will turn yellow (warning) at this threshold value (int)
        :param alarm_threshold: PRTG sensor will turn alarm (warning) at this threshold value (int)
        """
        self.name = name
        self.unit = unit
        self.primary = primary
        self.warning_threshold = warning_threshold
        self.warning_message = warning_message or '{0} degraded'.format(self.name)
        self.alarm_threshold = alarm_threshold
        self.alarm_message = alarm_message or '{0} severely degraded'.format(self.name)

    def to_json(self):
        """
        Output this channels configuration to JSON.
        :return: dict of config option which can be converted to JSON (dict)
        """
        config_dict = {'unit': self.unit, 'primary': 'true' if self.primary else '',
                       'warning_threshold': self.warning_threshold, 'warning_message': self.warning_message,
                       'alarm_threshold': self.alarm_threshold, 'alarm_message': self.alarm_message}
        return config_dict


class ActionConfiguration(object):

    def __init__(self, action):
        """
        Representation of a complete action config. When saving configurations, wrap an action in this Class and
        use 'to_json' to save the action configuration.
        :param action: Action object to represent configuration for (Action)
        """
        self.name = action.name
        self.base_action = action.base_action
        self.description = action.description
        self.channel_name = action.channel_name
        self.parameters = action.parameters

    def to_json(self):
        """
        Output this actions configuration to JSON.
        :return: dict of config option which can be converted to JSON (dict)
        """
        config_dict = {'base_action': self.base_action,
                       'description': self.description,
                       'channel_name': self.channel_name,
                       'parameters': {str(self._get_parameter_index(parameter))+'_'+parameter.name:
                                      {'description': parameter.description,
                                          'type': parameter.type,
                                          'value': parameter.value,
                                          'locked': parameter.locked}
                                      for parameter in self.parameters}}
        return config_dict

    def _get_parameter_index(self, parameter):

        for index, _parameter in enumerate(self.parameters):
            if _parameter is parameter:
                return index
