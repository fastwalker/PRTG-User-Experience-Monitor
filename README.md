# User experience monitor (PRTG sensor)

UEM is meant to be a quick and dirty tool for measuring the user experience of (remote) desktops. If the default
configuration is used, it can be up and running in 30 minutes requiring very little configuration.

By clicking template images on screen and sending key strokes to the (remote) desktop, UEM can measure
mouse- and keyboard latency and output it to a PRTG sensor through channels. Currently, (remote) desktops using the Windows
2012 or Windows 10 user interfaces are supported.

UEM works by running an agent on a PRTG probe and adding a 'Python script sensor' to that probe. When the script sensor
runs, it calls the agent which runs all actions (e.g. open Notepad, type in some keys, etc.). The latency of these
actions are returned to the PRTG sensor through channels (by default 'total run time', 'mouse latency' and
'keyboard latency'). This is achieved through image recognition and clicking screen locations, since this will work
over remote sessions when there is no direct control over the target OS.

There are far more advanced and precise tools available for this purpose, but they are expensive and often require much
more setup time. UEM can be up and running in 30 minutes and is free to use. The default behaviour of UEM at this early
stage is to open notepad through the start menu and type a Lorum Ipsum story into it, and sending back the mouse- and
keyboard latency to PRTG. It is possible to add custom actions through the GUI, such as logging in to a Citrix session
first, or opening different programs and clicking through their GUI's by adding custom 'Click image template' actions.

## Quick setup using defaults:

After following the below steps you will have a functioning UEM PRTG sensor, reporting on total run time of all actions,
keyboard latency and mouse latency, using the following actions:

- Open start menu (output channel: mouse latency);
- Type in 'Notepad';
- Open Notepad from the start menu (output channel: mouse latency);
- Type in a Lorum Ipsum story (output channel: keyboard latency);
- Click 'X' to close Notepad (output channel: mouse latency);
- Click 'Don't save' to close the unsaved warning (output channel: mouse latency).

![alt text](Images/doc/uem_sensor.JPG)

### 1. Requirements:

    - A functioning PRTG core server (trial works fine for testing purposes);
    - A (preferably dedicated) PRTG probe which can execute the actions, meaning
     its' GUI will be unavailable while running;
    - Microsoft Visual C++ 2010 installed on PRTG probe which runs UEM (see ./resources).

### 2. PRTG probe setup

    1. Unzip 'UEM client.zip' on a PRTG probe;
    2. Open 'Client.exe' and click the '3. Deployment' tab;
    3. Click 'Prepare python' to install the necessary libraries to PRTGs' builtin Python;
    4. Click 'Deploy script' to place the script sensor in %PRTG_ROOT_FOLDER\Custom sensors\Python;
    6. Click the '4. Run agent' tab and load the 'Notepad_latency_test' or 'Notepad_citrix_latency_test template';
    7. Only if using the Citrix template, click the '1. Input actions' tab and fill the username and password actions;
    8. Click the '4. run agent' tab, save the config, and click 'Start accepting requests' (PRTGs' script sensor can now call the agent to return results);
    9. Create the PRTG sensor (see 'PRTG Sensor setup') below.

### 3. PRTG sensor setup

    1. Log into PRTG and go to the 'Devices' tab;
    2. Right click the probe which will run UEM and click "Add sensor";
    3. Add a "Python script advanced" sensor;
    4. Choose the following settings (the rest may stay default):
         - Python script: "ScriptSensor.py";
         - Scanning interval: 5 minutes (1 minute works for the default setup, but leaves little breathing room);
    5. If the agent is started through 'UEM GUI.exe' on the probe, the sensor will start monitoring.

## UEM components:

Below the core components and concepts of UEM are explained.

### Script Sensor:

UEM uses a PRTG "Python custom script sensor' to output results to PRTG. The script sensor is placed in the
%PRTG ROOT%/Custom Sensors/Python folder, after which it can be selected while creating the sensor. When the sensor
starts running, it calls the script sensor. The script sensor will ask the agent to run all actions and return the
results. When the results are in, the script sensor will output them to PRTG.

### Agent:

The UEM agent (started through the UEM GUI) will execute actions on behalf of the script sensor and return the results.
Because PRTG will execute the script sensor in a non-interactive session, it cannot be used to take the required
screenshots for image recognition; therefore the agent must do this.

### Configuration templates:

From the client ('4. Run' tab), configuration templates can be loaded or created from the current configuration. Several
templates are available by default, such as the notepad_latency_test and its' Citrix variation,
notepad_citrix_latency_test. Use these for quick setup, but remember that some actions do require parameters to
be filled, such as Citrix login requiring a username and password.

### Channels:

Channels are a PRTG concept; each PRTG sensor can have multiple channels (e.g. cpu, memory, disk). UEM always has
a mandatory channel called 'Total run time', which contains the total run time of all actions.The default configuration
also has two extra channels called 'Mouse latency' and 'Keyboard latency'. Extra channels can be added by the user. PRTG
will pick them up automatically.

In UEM, a channel represents the average run time (in seconds or milliseconds) of all actions bound to that channel
(only actions that have validation can return a run time, as run time is defined as time from execution until time of validation).
For example, In the default config, all actions involving clicks are bound to the 'Mouse latency' channel, which means the average
click latency will be returned to PRTG under that channel.

Each channel also has a 'warning' and an 'alarm' threshold, which specifies at what value a warning or alarm is issued.
These values can be specified in milliseconds or seconds for each channel.

### Actions:

An action represents an automated user action, such as clicking the screen or sending keyboard strokes. Each action may
have (optional) parameters that control its' behavior. The fundamental actions of UEM are 'Click image template'
and 'Send keys'.

#### 'Click image template' action:

Try to locate the template image on the screen and click it. Optionally validate that the action was executed correctly by locating
the validation image on screen after clicking. This action has the following parameters:

- Image path (path to image to locate and click on screen, e.g. start_menu.jpg)
- Validation path (path to image to validate successful execution, e.g. opened_start_menu.jpg)
- Invert validation (validate that the validation image is NOT on screen, default behaviour is validating the image IS on screen)
- Number of clicks (amount of times to click the template image)
- Horizontal offset (click to the right or left of the template image)
- Vertical offset (click down or up from the template image)

#### 'Send keys' action

Send key strokes. Optionally validate they have been send by location the validation image
on screen. This actions has the following parameters:

- Keys (keys to send)
- Validation path (path to image to validate succeseful execution, e.g. opened_start_menu.jpg)

### Customization:

Actions and channels can be added through their respective tabs in the UEM GUI. Channels and most actions are
self explanatory by looking at their required parameters. The 'Click image template' action
requires some explanation.

#### Creating a custom 'Click image template' action:

This action requires a template image and optionally a validation image. When adding a custom action, add the
template and optional validation image to the "./Images/templates" folder. When filling the 'template image'
and 'validation image' parameters for the action, use only the image name.

For example, if you want to create an action that clicks the 'Microsoft Word' icon, place a file called
'word_icon.jpg' in the './UemImg/templates' folder, and fill in the template image parameter as
'word_icon.jpg'.

