# Standard libs
import ctypes
import logging
import win32ui
import win32gui
import datetime
import threading
from PIL import Image
from ctypes import windll
from dateutil.parser import parse
from collections import deque, OrderedDict
# Internal libs
import Config
# External libs
import os
import cv2
import time
import pyautogui


class Agent(object):

    def __init__(self, config=None):
        """
        The agent is responsible for actually executing the actions in Config.actions, e.g. image recognition,
        clicking the screen, sending keyboard strokes, etc. It requires a UemConfig object (containing actions,
        channels and parameters) to tell it what to do.

        When the PRTG sensor calls the SensorScript, it will create a file called run_request in the 'PRTG custom sensor
        python' folder. This objects' "run_request_handler" method will see that file, run all actions once, and write
        the results to a file called 'results_file' for the ScriptSensor to pick up and pass to PRTG.

        The results are obtained in the following way:
        After executing each action, it checks if the action has validation and a channel_name set; if it does,
        the run time of that action is added to that channel. After all actions have been executed, the list of
        action run times for each channel is averaged and returned through the 'run_once' method.

        To illustrate, imagine the following minimal config (only looking at channels and actions):
            - Channels:
                - Keyboard latency
                - Mouse latency
            - Actions:
                - Click start menu (channel: Mouse latency)
                - Open Notepad application (channel: None)
                - Type story in Notepad (channel: Keyboard latency)
        When executed, the time it takes from clicking the start menu until it opens will appear in PRTG under the
        'Mouse latency' channel. The time it takes from sending the keystrokes to Word until the story is fully on
        screen will be returned to PRTG as 'Keyboard latency'.

        :param config (UemConfig object)
        """
        self.logger = logging.getLogger('root')
        self.logger.info('Initializing agent')

        self.config = config or Config.Config()

        self.running = False
        self.start_time = None
        self.handle_requests = False  # Setting this to False after calling 'run_request_handler' will stop it
        self.last_results = None

        self.requests_thread = threading.Thread(target=self.run_request_handler, daemon=True)
        self.requests_thread.start()

    @property
    def timed_out(self):
        """
        :return: True if agent is running and current_time - start_time > self.config.agent_timeout
        """
        if self.running:
            if (datetime.datetime.now() - self.start_time).seconds >= self.config.agent_timeout:
                return True

    def run_request_handler(self):
        """
        Method meant to be run in a background thread. Scans the PRTG custom sensor python folder for a file named
        'run_request', which will be created by the ScriptSensor when PRTG runs it. Once it exists and is not older than
        a minute, the agent will run all actions and write the result to a file named 'run_results' in the same folder.
        """
        self.logger.info('Start monitoring requests')
        request_file_path = os.path.join(self.config.prtg_python_sensor_script_folder, 'run_request')
        results_file_path = os.path.join(self.config.prtg_python_sensor_script_folder, 'run_results')

        while True:
            if not self.handle_requests:
                time.sleep(1)
                continue
            if os.path.exists(request_file_path) and not self.running:
                self._handle_run_request(request_file_path=request_file_path, results_file_path=results_file_path)
            else:
                time.sleep(0.5)

    def run_once(self):
        """
        Run the UemConfig.actions list once and return the average run time of actions for each channel in
        UemConfig.Channels in the follow form: {'channel_name': average_action_run_time} for each channel, where
        the actions refers to those actions which have 'channel_name' set as their output channel.
        :return: {'channel_name': average_action_run_time} (dict)
        """
        self.logger.info('Starting run.')

        self.running = True
        self.start_time = datetime.datetime.now()

        try:
            run_results = self._execute_actions()
        except Exception as e:
            self.running = False
            self.last_results = str(e)
            self.logger.error('Run failed: "{0}"'.format(e))
            return
        else:
            self.running = False
            self.last_results = run_results
            self.logger.info('Completed run, results: {0}'.format(run_results))
            return run_results

    def find_image_template(self, template_path, wait_for_template=True, wait_duration=15, restrict_to_window=None):
        """
        Locate the passed image template on the current screen.
        :param template_path: path to the image file to search for on screen (str)
        :param wait_for_template: if the template is not found keep trying until it appears (Bool)
        :param wait_duration: how many seconds to wait (int)
        :param restrict_to_window: only look for template in the application window containing this name (str)
        """
        self.logger.debug('Attempting to find template image {0}, wait={1}, remaining duration={2}.'.format(
            template_path, wait_for_template, wait_duration))
        try:
            self._get_image_template_coordinates(template_path=template_path, restrict_to_window=restrict_to_window)
        except RuntimeError as e:
            if wait_for_template and not self.timed_out and wait_duration:
                wait_duration -= 1
                time.sleep(0.5)
                return self.find_image_template(template_path=template_path, wait_for_template=wait_for_template,
                                                restrict_to_window=restrict_to_window, wait_duration=wait_duration)
            else:
                raise e

    def click_image_template(self, template_path, wait_for_template=True, wait_duration=15, number_of_clicks=1,
                             offset=(0, 0), restrict_to_window=None):
        """
        Locate the passed image template on the current screen and click that position.
        :param template_path: path to the image file to search for on screen (str)
        :param wait_for_template: if the template is not found keep trying until it appears (Bool)
        :param wait_duration: how many seconds to wait (int)
        :param number_of_clicks (int)
        :param offset: offset the click relative to the found image template ((x, y) tuple of int)
        :param restrict_to_window: only look for template in the application window containing this name (str)
        """
        self.logger.debug('Attempting to click template image {0}, wait={1}, remaining duration={2}.'.format(
            template_path, wait_for_template, wait_duration))
        try:
            self._click_coordinates(
                coordinates=self._get_image_template_coordinates(template_path=template_path,
                                                                 restrict_to_window=restrict_to_window),
                number_of_clicks=number_of_clicks, offset=offset)
        except RuntimeError as e:
            if wait_for_template and not self.timed_out and wait_duration:
                wait_duration -= 1
                time.sleep(1)
                return self.click_image_template(
                    template_path=template_path, wait_for_template=wait_for_template, offset=offset,
                    number_of_clicks=number_of_clicks, restrict_to_window=restrict_to_window,
                    wait_duration=wait_duration)
            else:
                raise e

    def ensure_image_template_not_present(self, template_path, wait_for_template_not_present=True, wait_duration=15,
                                          restrict_to_window=None):
        """
        Ensure the passed image template is not on the current screen. (reverse of 'find_template')
        :param template_path: path to the image file to search for on screen (str)
        :param wait_for_template_not_present: if the template is found keep trying until it disappears (Bool)
        :param wait_duration: how many seconds to wait (int)
        :param restrict_to_window: only look for template in the application window containing this name (str)
        """
        self.logger.debug('Ensuring template image {0} not on screen, wait={1}, remaining duration={2}'.format(
            template_path, wait_for_template_not_present, wait_duration))
        try:
            self.find_image_template(template_path=template_path, wait_for_template=False,
                                     restrict_to_window=restrict_to_window)
        except RuntimeError:  # The image must NOT be on screen, so an error is expected in this case
            return True
        else:
            if wait_for_template_not_present and not self.timed_out and wait_duration:
                wait_duration -= 1
                time.sleep(1)
                self.ensure_image_template_not_present(template_path=template_path, wait_duration=wait_duration,
                                                       wait_for_template_not_present=wait_for_template_not_present,
                                                       restrict_to_window=restrict_to_window)
            else:
                raise RuntimeError('Template found on screen.')

    def send_keys(self, keys):
        """
        Input keystrokes.
        :param keys (str)
        """
        self.logger.debug('Sending keys {0}'.format(keys))
        pyautogui.typewrite(keys)

    def press_key(self, key):
        """
        Press a function key.
        :param key (str)
        """
        self.logger.debug('Pressing key {0}'.format(key))
        pyautogui.press(key)

    def _execute_actions(self):
        """
        Execute all actions and return a results dict.
        :return: {'channel_name': average_action_run_time} (dict)
        """
        self.logger.info('Executing all actions.')
        # run_results = {channel_name: [] for channel in UemConfig.channels} used to store action run times
        run_results = OrderedDict({key: deque() for key in self.config.channels.keys()})

        for action in self.config.actions:
            while action.retry:
                if self.timed_out:
                    raise RuntimeError("Timed out while executing action {0}".format(action.name))
                try:
                    self._execute_action(action, results=run_results)
                except Exception as e:
                    if action.retry:
                        action.retry -= 1
                        self.logger.debug("Retrying action '{0}'. Retries left: '{1}'".format(action.name, action.retry))
                        continue
                    else:
                        raise RuntimeError("Error while executing action '{0}': '{1}'".format(action.name, e))
                else:
                    break

        run_total = int((datetime.datetime.now() - self.start_time).seconds)

        run_results = {channel_name: self._get_run_time_average(channel_values)
                       for channel_name, channel_values in run_results.items()}
        run_results['Run total'] = run_total
        return run_results

    def _execute_action(self, action, results):
        """
        Execute and optionally validate an action and if it has been validated, add the run time to the passed
        results dict.
        :param action (Action object)
        :param results: ({channel_name: []} dict for each channel in UemConfig.channels)
        """
        self.logger.info('Executing action {0}'.format(action.name))
        action.execute(agent=self)
        if action.validate_after_execution and not self.timed_out:
            action.validate(agent=self)
            if action.channel_name:
                unit = self.config.channels[action.channel_name].unit
                results[action.channel_name].append(action.run_time(unit=unit))

    @staticmethod
    def _get_run_time_average(values):
        """
        Used to average the action run times of a channel.
        :param values (list of int or float)
        :return (int)
        """
        if len(values) == 0 or sum(values) == 0:
            return 0
        return int(sum(values) / len(values))

    def _get_image_template_coordinates(self, template_path, error_limit=0.75, restrict_to_window=None):
        """
        Find the passed template image on screen and return it's coordinates.
        :param template_path: path to template image to find (str)
        :param error_limit: lower certainty limit before throwing an exception (float between 0 and 1)
        :param restrict_to_window: only look for template in the application window containing this name (str)
        :return: coordinates tuple, e.g. (250, 100) (Tuple of ints)
        """
        img_name = template_path
        template_path = os.path.join('Images/templates', template_path)
        if not os.path.exists(template_path):
            raise FileNotFoundError("Template image path does not exist: {0}".format(template_path))

        self.logger.debug('Finding image template coordinates for: {0}'.format(template_path))

        x_offset, y_offset = 0, 0
        if not restrict_to_window:
            screenshot = self._take_screenshot()
        else:
            screenshot, x_offset, y_offset = self._take_screenshot_of_window(window_name=restrict_to_window)

        template = self._read_template(template_path)

        res = cv2.matchTemplate(screenshot, template, cv2.TM_CCOEFF_NORMED)
        self.logger.debug('Cv2 template matching result: {0}'.format(res))
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        if max_val < error_limit:
            self.logger.debug('Could not find image in template. Certainty: {0}'.format(max_val))
            raise RuntimeError("Could not find template {0} in screenshot.".format(img_name))

        self.logger.debug('Found image in template. Certainty: {0}, location: {1}'.format(max_val, max_loc))
        return max_loc[0] + x_offset, max_loc[1] + y_offset

    def _prepare_for_screenshot(self, path):

        if os.path.exists(path):
            self.logger.debug('Exists, removing path: {0}'.format(path))
            os.remove(path)
        elif not os.path.exists(os.path.dirname(path)):
            self.logger.debug('Does not exist, creating folder: {0}'.format(path))
            os.mkdir(os.path.dirname(path))

    def _take_screenshot(self, path='Images/temp/screenshot.jpg'):
        """
        Save the current screen as a temporary image, which can then be used to locate image templates in.
        :param path: temp path to save screenshot to (str)
        :return: in memory image object (cv2.Image object)
        """
        path = os.path.join(self.config.uem_root_folder, path)
        self._prepare_for_screenshot(path)

        pyautogui.screenshot(path)
        screenshot = cv2.imread(path, 0)
        return screenshot

    def _take_screenshot_of_window(self, window_name, path='Images/temp/screenshot.jpg'):
        """
        Save the passed window name as a temporary image, which can then be used to locate image templates in.
        Return a screenshot and the horizontal and vertical offset needed to reach the passed window name. E.g.,
        if the topleft of the window is located at (100, 100), return those values. If the image template is located at
        (100, 100) in the window, the absolute position of the image will be (200, 200).
        :param path: temp path to save screenshot to (str)
        :return: in memory image object (cv2.Image object), x_offset (int), y_offset (int)
        """
        self.logger.debug("Taking screenshot of window with name: {0}".format(window_name))
        #  https://stackoverflow.com/questions/19695214/python-screenshot-of-inactive-window-printwindow-win32gui
        path = os.path.join(self.config.uem_root_folder, path)
        self._prepare_for_screenshot(path)

        window_name = self.find_window(window_name)
        hwnd = win32gui.FindWindow(None, window_name)

        left, top, right, bot = win32gui.GetWindowRect(hwnd)
        w = right - left
        h = bot - top

        hwndDC = win32gui.GetWindowDC(hwnd)
        mfcDC  = win32ui.CreateDCFromHandle(hwndDC)
        saveDC = mfcDC.CreateCompatibleDC()

        saveBitMap = win32ui.CreateBitmap()
        saveBitMap.CreateCompatibleBitmap(mfcDC, w, h)

        saveDC.SelectObject(saveBitMap)

        result = windll.user32.PrintWindow(hwnd, saveDC.GetSafeHdc(), 0)

        bmpinfo = saveBitMap.GetInfo()
        bmpstr = saveBitMap.GetBitmapBits(True)

        im = Image.frombuffer(
            'RGB',
            (bmpinfo['bmWidth'], bmpinfo['bmHeight']),
            bmpstr, 'raw', 'BGRX', 0, 1)

        win32gui.DeleteObject(saveBitMap.GetHandle())
        saveDC.DeleteDC()
        mfcDC.DeleteDC()
        win32gui.ReleaseDC(hwnd, hwndDC)

        if result == 1:
            im.save(path)
        screenshot = cv2.imread(path, 0)
        left, top = left if left >= 0 else 0, top if top >= 0 else 0
        self.logger.debug("Returning screen of window {0} @ position ({1}, {2})".format(window_name, left, top))
        return screenshot, left, top

    def find_window(self, name):
        """
        Fuzzy search for a window name; e.g. 'Citrix receiver' would return the 'Company X - Citrix receiver' window if
        it exists.
        :param name (str)
        :return: full name (str)
        """
        self.logger.debug("Finding window with (partial) name: {0}".format(name))
        #  https://sjohannes.wordpress.com/2012/03/23/win32-python-getting-all-window-titles/
        EnumWindows = ctypes.windll.user32.EnumWindows
        EnumWindowsProc = ctypes.WINFUNCTYPE(ctypes.c_bool, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int))
        GetWindowText = ctypes.windll.user32.GetWindowTextW
        GetWindowTextLength = ctypes.windll.user32.GetWindowTextLengthW
        IsWindowVisible = ctypes.windll.user32.IsWindowVisible

        titles = []

        def foreach_window(hwnd, lParam):
            if IsWindowVisible(hwnd):
                length = GetWindowTextLength(hwnd)
                buff = ctypes.create_unicode_buffer(length + 1)
                GetWindowText(hwnd, buff, length + 1)
                titles.append(buff.value)
            return True
        EnumWindows(EnumWindowsProc(foreach_window), 0)
        for title in titles:
            if name.lower() in title.lower():
                self.logger.debug("Found full window name: {0}".format(title))
                return title

    def _read_template(self, path):
        """
        Return the passed image path as an in memory image object.
        :param path (str)
        :return: in memory image object (cv2.Image object)
        """
        self.logger.debug('Reading template image into memory: {0}'.format(path))
        return cv2.imread(path, 0)

    def _click_coordinates(self, coordinates, offset=(0, 0), number_of_clicks=1):
        """
        Click the passed coordinates on screen.
        :param coordinates: coordinates tuple, e.g. (250, 100) (Tuple of ints)
        :param offset: offset the coordinates ((x, y) tuple of ints)
        """
        coordinates = (coordinates[0]+offset[0], coordinates[1]+offset[1])
        self.logger.debug('Clicking coordinates {0} ({1} times)'.format(coordinates,
                                                                                           number_of_clicks))
        # We need to move the mouse around a little bit to nudge Windows into reacting, especially win2012 start menu
        pyautogui.moveTo(1, 1)
        pyautogui.moveTo(coordinates[0] + 10, coordinates[1] + 10)
        pyautogui.moveTo(coordinates[0] - 10, coordinates[1] - 10)

        pyautogui.moveTo(*coordinates)
        pyautogui.click(*coordinates)

    def _write_run_results_file(self, path, results):
        """
        Write the results of a run to the passed output path, in a format ScriptSensor.py will understand, so it can
        read the results and return it to PRTG.
        :param path (str)
        :param results: {channel_name: average_action_run_time} dict containing each Config.channels channel (dict)
        """
        with open(path, 'w') as ofile:
            if not results:
                ofile.write("ERROR")
            else:
                ofile.write("\n".join(["{0},{1},{2},{3},{4},{5}".format(
                    channel_name,
                    channel_result,
                    self.config.channels[channel_name].unit,
                    'true' if self.config.channels[channel_name].primary else '',
                    self.config.channels[channel_name].warning_threshold,
                    self.config.channels[channel_name].alarm_threshold)
                    for channel_name, channel_result in results.items()]))

    def _handle_run_request(self, request_file_path, results_file_path):
        """
        Handling the passed run request by determining if it is valid, and if it is run all actions once and write the
        results to the passed output path.
        :param request_file_path (str)
        :param results_file_path (str)
        """
        self.logger.debug('Received run request, start processing')
        if not self._validate_request(request_file_path):
            return os.remove(os.path.join(self.config.prtg_python_sensor_script_folder, 'run_request'))
        else:
            os.remove(os.path.join(self.config.prtg_python_sensor_script_folder, 'run_request'))
            results = self.run_once()
            self._write_run_results_file(path=results_file_path, results=results)

    def _validate_request(self, request_file_path):
        """
        Determine if a run request is too old to execute.
        :param request_file_path (str)
        :return: False if expired, True if valid (Bool)
        """
        with open(os.path.join(request_file_path), 'r') as ofile:
            request_time = parse(ofile.read().strip())
        if (datetime.datetime.now() - request_time).seconds > 60:
            self.logger.error('Request expired: {0}'.format(request_time))
            return False
        else:
            return True
