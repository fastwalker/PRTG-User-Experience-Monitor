# Standard libs
from logging.handlers import RotatingFileHandler
import subprocess
import logging
import base64
import shutil
import os
from cryptography import fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC


def start_logging(log_path='./UEM.log', level=logging.DEBUG):
    """
    :param log_path (str)
    """
    formatter = logging.Formatter('[%(asctime)s] [%(levelname)s] [%(module)s %(funcName)s(%(lineno)d)] %(message)s')
    handler = RotatingFileHandler(log_path, mode='w', maxBytes=2*1024*1024, backupCount=2, encoding=None, delay=0)
    handler.setFormatter(formatter)
    handler.setLevel(level=level)
    global LOGGER
    LOGGER = logging.getLogger('root')
    LOGGER.setLevel(level)
    LOGGER.addHandler(handler)


def deploy_python_sensor_script(uem_root_path, prtg_custom_sensor_python_path):
    """
    Copy all files required for the 'PRTG script sensor' to work to the 'PRTG Python custom sensor script folder'.
    It is normally located at %prtg_root_folder%/Custom Sensors/Python.
    :param prtg_custom_sensor_python_path (str)
    :param uem_root_path (str)
    """
    LOGGER.info('Deploying script sensor from "{0}" to "{1}"'.format(
        uem_root_path, prtg_custom_sensor_python_path))

    safe_copy(src=os.path.join(uem_root_path, 'ScriptSensor.py'),
              dst=os.path.join(prtg_custom_sensor_python_path, 'ScriptSensor.py'))
    write_prtg_custom_sensor_path_to_script_sensor(script_sensor_path=prtg_custom_sensor_python_path)


def write_prtg_custom_sensor_path_to_script_sensor(script_sensor_path):
    """
    Hard code the path to the PRTG custom folder in the ScriptSensor (as PRTG does not set CWD to that folder
    at run time).
    """
    LOGGER.debug('Hard coding sensor path in sensor script: {0}'.format(script_sensor_path))
    script_sensor_executable_path = os.path.join(script_sensor_path, 'ScriptSensor.py')
    with open(script_sensor_executable_path, 'r') as ofile:
        code = ofile.read()

    os.remove(script_sensor_executable_path)
    code = code.replace("{{CUSTOM_SCRIPT_PATH}}", script_sensor_path)

    with open(script_sensor_executable_path, 'w') as ofile:
        ofile.write(code)


def safe_copy(src, dst):
    """
    Copy files and remove dst if it exists (shutil cannot do this).
    :param src (str)
    :param dst (str)
    """
    LOGGER.debug('Copying "{0}" to "{1}"'.format(src, dst))
    if os.path.exists(dst):
        os.remove(dst)
    shutil.copyfile(src=src, dst=dst)


def prepare_uem_root_path(uem_root_path=None):
    """
    Check if the passed path is a valid PRTG client path by checking for UEM relevant PRTG folders.
    :param uem_root_path (str)
    :return (Bool)
    """
    uem_root_path = uem_root_path or os.path.dirname(__file__)
    LOGGER.debug('Preparing UEM root path "{0}"'.format(uem_root_path))
    if not os.path.exists(uem_root_path) and 'Actions' in os.listdir(uem_root_path):
        os.mkdir(os.path.join(uem_root_path, 'Actions'))
    if not os.path.exists(uem_root_path) and 'Images' in os.listdir(uem_root_path):
        os.mkdir(os.path.join(uem_root_path, 'Images'))
    if not os.path.exists(uem_root_path) and 'Templates' in os.listdir(uem_root_path):
        os.mkdir(os.path.join(uem_root_path, 'Templates'))
    if not os.path.exists(uem_root_path) and 'Requirements' in os.listdir(uem_root_path):
        os.mkdir(os.path.join(uem_root_path, 'Requirements'))


def prtg_root_path_is_valid(prtg_root_path):
    """
    Check if the passed path is a valid PRTG client path by checking for UEM relevant PRTG folders.
    :param prtg_root_path (str)
    :return (Bool)
    """
    LOGGER.debug('Determining if "{0}" is valid PRTG root path"'.format(prtg_root_path))
    if os.path.exists(prtg_root_path) and 'Python34' in os.listdir(prtg_root_path) and \
            'Custom Sensors' in os.listdir(prtg_root_path):
        return True


def sensor_script_is_deployed(prtg_root_path):
    """
    Check if SensorScript.py is present in the PRTG root path and if it is identical to the temlate.
    :param prtg_root_path (str)
    """
    LOGGER.debug('Determining if sensor script is deployed to "{0}" and up to date"'.format(
        prtg_root_path))
    prtg_sensor_script_folder = os.path.join(prtg_root_path, 'Custom sensors', 'Python')
    if not os.path.exists(prtg_root_path) or not os.path.exists(prtg_sensor_script_folder) or \
            'ScriptSensor.py' not in os.listdir(prtg_sensor_script_folder):
        return False
    else:
        with open(os.path.join(prtg_sensor_script_folder, 'ScriptSensor.py')) as ofile:
            template_text = ofile.read().replace('{{CUSTOM_SCRIPT_PATH}}',
                                                 prtg_sensor_script_folder)
        with open(os.path.join(prtg_sensor_script_folder, 'ScriptSensor.py')) as ofile:
            deployed_text = ofile.read()
        if template_text == deployed_text:
            return True
        else:
            return False


def get_python_installed_packages(pip_executable_path):
    """
    Get all python packages installed through pip. All python requirements for UEM are installed through pip, so this list
    is representative of the state of the requirements. Use 'python_pip_is_installed' to determine if pip is available
    before calling this function to prevent errors.
    :param pip_executable_path (str)
    :return: "pip list" output, i.e. newline seperated list of packages (str)
    """
    LOGGER.debug('Getting installed python packages for: {0}'.format(pip_executable_path))
    output = subprocess.check_output('{0} list'.format(pip_executable_path),
                                     stdin=subprocess.PIPE, stderr=subprocess.PIPE).decode().strip()
    LOGGER.debug('Installed packages output: {0}'.format(output))
    return output


def python_requirements_are_valid(python_executable_path, pip_executable_path=None, install_requirements=False,
                                  progress_bar=None):
    """
    Check if all python requirements are met for the passed python and pip executables. Requirements are:
    - pip (for checking and installing requirements)
    - Pillow (for taking screenshots and loading template images through PIL.Image)
    - pyautogui (for controlling keyboard and mouse)
    - Opencv-python (for finding template images on screen)
    :param python_executable_path (str)
    :param pip_executable_path (str)
    :param install_requirements: try to install missing requirements if True (Bool)
    :param progress_bar: optional Ttk.ProgressBar to update while installing (tk.ProgressBar)
    :return: True if requirements are met, else False (Bool)
    """
    if not pip_executable_path:
        pip_executable_path = os.path.split(python_executable_path)[0]
        pip_executable_path = os.path.join(pip_executable_path, '/scripts/pip.exe')

    logging.log(level=logging.INFO,
                msg='Checking package requirements for python @ {0} and pip @ {1}. Install missing is set to: {2}'
                .format(python_executable_path, pip_executable_path, install_requirements))

    if not pip_installed(pip_executable_path=pip_executable_path):
        if not install_requirements:
            return False
        else:
            try:
                install_python_pip(python_executable_path=python_executable_path)
            except subprocess.CalledProcessError:
                return False
    if progress_bar:
        progress_bar.step(25)
        progress_bar.update()

    pip_output = get_python_installed_packages(pip_executable_path=pip_executable_path)  # Cache to save pip calls
    if not python_pillow_installed(pip_output=pip_output):
        if not install_requirements:
            return False
        else:
            try:
                install_python_pillow(pip_executable_path=pip_executable_path)
            except subprocess.CalledProcessError:
                return False
    if progress_bar:
        progress_bar.step(25)
        progress_bar.update()

    if not python_pyautogui_installed(pip_output=pip_output):
        if not install_requirements:
            return False
        else:
            try:
                install_python_pyautogui(pip_executable_path=pip_executable_path)
            except subprocess.CalledProcessError:
                return False
    if progress_bar:
        progress_bar.step(25)
        progress_bar.update()

    if not python_cv2_installed(pip_output=pip_output):
        if not install_requirements:
            return False
        else:
            try:
                install_python_cv2(pip_executable_path=pip_executable_path)
            except subprocess.CalledProcessError:
                return False
    if progress_bar:
        progress_bar.step(25)
        progress_bar.update()

    LOGGER.info('Python @ {0} meets all requirements'.format(python_executable_path))
    return True


def python_package_installed(package_name, pip_executable_path=None, pip_output=None):
    """
    Check if the a python package is installed by checking 'pip list' output.
    Provide either the path to pip so the command can be run, or the output of the command if it has already
    been run (useful for caching command output and preventing multiple pip calls).
    :param package_name (str)
    :param pip_executable_path: optional (str)
    :param pip_output: "pip list" output
    :return (Bool)
    """
    LOGGER.debug('Determining if {0} is installed'.format(package_name))
    pip_output = pip_output or get_python_installed_packages(pip_executable_path)
    if package_name in pip_output:
        LOGGER.info('{0} package is currently installed'.format(package_name))
        return True
    else:
        LOGGER.info('{0} package is currently NOT installed'.format(package_name))


def pip_installed(pip_executable_path):
    """
    :param pip_executable_path (str)
    :return: True if installed, False if not (Bool)
    """
    try:
        get_python_installed_packages(pip_executable_path)
    except Exception as e:
        LOGGER.info('pip is currently NOT installed: error "{0}"'.format(e))
        return False
    else:
        return True


def python_pillow_installed(pip_executable_path=None, pip_output=None):
    """
    :param pip_executable_path (str)
    :param pip_output: 'pip.exe list' output (str)
    :return: True if installed, False if not (Bool)
    """
    return python_package_installed(pip_executable_path=pip_executable_path, pip_output=pip_output,
                                    package_name='Pillow')


def python_pyautogui_installed(pip_executable_path=None, pip_output=None):
    """
    :param pip_executable_path (str)
    :param pip_output: 'pip.exe list' output (str)
    :return: True if installed, False if not (Bool)
    """
    return python_package_installed(pip_executable_path=pip_executable_path, pip_output=pip_output,
                                    package_name='PyAutoGUI')


def python_cv2_installed(pip_executable_path=None, pip_output=None):
    """
    :param pip_executable_path (str)
    :param pip_output: 'pip.exe list' output (str)
    :return: True if installed, False if not (Bool)
    """
    return python_package_installed(pip_executable_path=pip_executable_path, pip_output=pip_output,
                                    package_name='opencv-python')


def install_python_pip(python_executable_path):
    """
    Install pip through the bootstrap script in ./Requirements/get-pip.py.
    :param python_executable_path (str)
    """
    try:
        output = subprocess.check_output('{0} ./Requirements/get-pip.py'.format(python_executable_path))
        LOGGER.debug('Pip install output: "{0}"'.format(output))
    except subprocess.CalledProcessError as e:
        LOGGER.debug('Pip install failed: "{0}"'.format(e))
    else:
        LOGGER.info('Pip was installed')


def install_python_package(pip_executable_path, package_name):
    """
    Install a python package through pip.
    :param pip_executable_path (str)
    :param package_name (str)
    """
    try:
        output = subprocess.check_output('{0} install {1}'.format(pip_executable_path, package_name))
        LOGGER.debug('{0} install output: "{1}"'.format(package_name, output))
    except subprocess.CalledProcessError as e:
        LOGGER.debug('{0} install failed: "{1}"'.format(package_name, e))
    else:
        LOGGER.info('{0} was installed'.format(package_name))


def install_python_pillow(pip_executable_path):
    """
    Install pillow package through pip.
    :param pip_executable_path (str)
    """
    install_python_package(pip_executable_path=pip_executable_path, package_name='Pillow')


def install_python_pyautogui(pip_executable_path):
    """
    Install pyautogui package through pip.
    :param pip_executable_path (str)
    """
    install_python_package(pip_executable_path=pip_executable_path, package_name='pyautogui')


def install_python_cv2(pip_executable_path):
    """
    Install cv2 package through pip.
    :param pip_executable_path (str)
    """
    install_python_package(pip_executable_path=pip_executable_path, package_name='opencv-python')


def config_is_encrypted(config_path='./config'):

    try:
        with open(config_path, 'r') as open_file:
            assert open_file.read().startswith('{')
    except UnicodeDecodeError:
        return True
    except AssertionError:
        return True
    else:
        return False


def validate_decryption_key(key, encrypted_config_path='./config'):

    try:
        read_encrypted_file(key_string=key, path=encrypted_config_path)
    except fernet.InvalidToken:
        return False
    else:
        return True


def write_encrypted_file(data_string, key_string, path):

    with open(path, 'wb') as open_file:
        token, salt = _encrypt(data_string=data_string, key_string=key_string)
        open_file.write(salt+token)


def read_encrypted_file(key_string, path):

    with open(path, 'rb') as open_file:
        raw = open_file.read()
    salt = raw[0:16]
    token = raw[16:]
    data = _decrypt(token=token, salt=salt, key_string=key_string)
    return data


def _encrypt(data_string, key_string):

    f, salt = _generate_fernet_key(key_string)
    return f.encrypt(data_string.encode()), salt


def _decrypt(token, salt, key_string):

    f, salt = _generate_fernet_key(key_string=key_string, salt=salt)
    return f.decrypt(token)


def _generate_fernet_key(key_string, salt=None):

    salt = salt or os.urandom(16)
    kdf = PBKDF2HMAC(algorithm=hashes.SHA256(), length=32, salt=salt, iterations=100000, backend=default_backend())
    key = base64.urlsafe_b64encode(kdf.derive(key_string.encode()))
    return fernet.Fernet(key), salt
